package com.ukefu.webim.web.handler.admin.system;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder.Operator;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.repository.OnlineUserRepository;
import com.ukefu.webim.service.repository.RequestLogRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.RequestLog;
import com.ukefu.webim.web.model.UKeFuDic;

@Controller
@RequestMapping("/admin/reqlog")
public class RequestLogController extends Handler{
	
	
	@Autowired
	private RequestLogRepository requestLogRes;
	
	@Autowired
	private UserRepository userRes ;
	
	@Autowired
	private OnlineUserRepository onlineUserRes ;
	
    @RequestMapping("/index")
    @Menu(type = "admin" , subtype = "reqlog" ,spadmin = true)
    public ModelAndView index(final ModelMap map ,final HttpServletRequest request) {
//    	final String orgi = super.getOrgi(request);
//    	Page<RequestLog> requestLogList = requestLogRes.search(query, new PageRequest(super.getP(request), super.getPs(request) , Direction.DESC , "createdate")) ;
    	
//    	map.addAttribute("requestLogList", requestLogList) ;
    	Page<RequestLog> requestLogPage = requestLogRes.findAll(new Specification<RequestLog>(){
			@Override
			public Predicate toPredicate(Root<RequestLog> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  

//				list.add(cb.equal(root.get("orgi").as(String.class), orgi));
				
				//搜索框
				if(!StringUtils.isBlank(request.getParameter("q"))) {
					String q = request.getParameter("q") ;
					q = q.replaceAll("(OR|AND|NOT|:|\\(|\\))", "") ;
					if(!StringUtils.isBlank(q)){
						map.put("q", q) ;
						q = "%"+q+"%";
						list.add(cb.or(cb.like(root.get("name").as(String.class), q)
								,cb.like(root.get("username").as(String.class), q)
								,cb.like(root.get("usermail").as(String.class), q)
								,cb.like(root.get("url").as(String.class), q)
								,cb.like(root.get("classname").as(String.class), q)
								,cb.like(root.get("methodname").as(String.class), q)
								,cb.like(root.get("filename").as(String.class), q)
								,cb.like(root.get("throwable").as(String.class), q)));
					}
				}
				
				//操作人
				if(!StringUtils.isBlank(request.getParameter("username"))) {
					list.add(cb.equal(root.get("username").as(String.class), request.getParameter("username")));
					map.put("username", request.getParameter("username")) ;
				}
				
				//操作人员ID
				if(!StringUtils.isBlank(request.getParameter("userid"))) {
					list.add(cb.equal(root.get("userid").as(String.class), request.getParameter("userid")));
					map.put("userid", request.getParameter("userid")) ;
				}
				
				//操作人员的EMAIL
				if(!StringUtils.isBlank(request.getParameter("usermail"))) {
					list.add(cb.equal(root.get("usermail").as(String.class), request.getParameter("usermail")));
					map.put("usermail", request.getParameter("usermail")) ;
				}
				
				//操作url
				if(!StringUtils.isBlank(request.getParameter("url"))) {
					list.add(cb.like(root.get("url").as(String.class), "%"+request.getParameter("url")+"%"));
					map.put("url", request.getParameter("url")) ;
				}
				
				//传入参数
				if(!StringUtils.isBlank(request.getParameter("parameters"))) {
					list.add(cb.like(root.get("parameters").as(String.class), "%"+request.getParameter("parameters")+"%"));
					map.put("parameters", request.getParameter("parameters")) ;
				}
				
				//局域网IP
				if(!StringUtils.isBlank(request.getParameter("reportname"))) {
					list.add(cb.like(root.get("reportname").as(String.class), "%"+request.getParameter("reportname")+"%"));
					map.put("reportname", request.getParameter("reportname")) ;
				}
				
				//
				if(!StringUtils.isBlank(request.getParameter("detailtype"))) {
					list.add(cb.equal(root.get("detailtype").as(String.class), request.getParameter("detailtype")));
					map.put("detailtype", request.getParameter("detailtype")) ;
				}
				
				//ip地址
				if(!StringUtils.isBlank(request.getParameter("ip"))) {
					list.add(cb.like(root.get("ip").as(String.class), "%"+request.getParameter("ip")+"%"));
					map.put("ipp", request.getParameter("ip")) ;
				}
				
				//hostname
				if(!StringUtils.isBlank(request.getParameter("hostname"))) {
					list.add(cb.like(root.get("hostname").as(String.class), "%"+request.getParameter("hostname")+"%"));
					map.put("hostnamee", request.getParameter("hostname")) ;
				}
				
				//
				if(!StringUtils.isBlank(request.getParameter("statues"))) {
					list.add(cb.equal(root.get("statues").as(String.class), request.getParameter("statues")));
					map.put("statues", request.getParameter("statues")) ;
				}
				
				//
				if(!StringUtils.isBlank(request.getParameter("triggerwarning"))) {
					list.add(cb.like(root.get("triggerwarnings").as(String.class), "%"+request.getParameter("triggerwarning")+"%"));
					map.put("triggerwarning", request.getParameter("triggerwarning")) ;
				}
				
				//类名
				if(!StringUtils.isBlank(request.getParameter("classname"))) {
					list.add(cb.like(root.get("classname").as(String.class), "%"+request.getParameter("classname")+"%"));
					map.put("classname", request.getParameter("classname")) ;
				}
				
				//方法名
				if(!StringUtils.isBlank(request.getParameter("methodname"))) {
					list.add(cb.like(root.get("methodname").as(String.class), "%"+request.getParameter("methodname")+"%"));
					map.put("methodname", request.getParameter("methodname")) ;
				}
				
				//字段名
				if(!StringUtils.isBlank(request.getParameter("filename"))) {
					list.add(cb.like(root.get("filename").as(String.class), "%"+request.getParameter("filename")+"%"));
					map.put("filename", request.getParameter("filename")) ;
				}
				
				//行号
				if(!StringUtils.isBlank(request.getParameter("linenumber"))) {
					list.add(cb.equal(root.get("linenumber").as(String.class), request.getParameter("linenumber")));
					map.put("linenumber", request.getParameter("linenumber")) ;
				}
				
				//线程
				if(!StringUtils.isBlank(request.getParameter("throwable"))) {
					list.add(cb.equal(root.get("throwable").as(String.class), request.getParameter("throwable")));
					map.put("throwable", request.getParameter("throwable")) ;
				}
				
				//Menu type
				if(!StringUtils.isBlank(request.getParameter("funtype"))) {
					list.add(cb.equal(root.get("funtype").as(String.class), request.getParameter("funtype")));
					map.put("funtype", request.getParameter("funtype")) ;
				}
				
				//Menu subtype
				if(!StringUtils.isBlank(request.getParameter("fundesc"))) {
					list.add(cb.equal(root.get("fundesc").as(String.class), request.getParameter("fundesc")));
					map.put("fundesc", request.getParameter("fundesc")) ;
				}
				
				//error
				if(!StringUtils.isBlank(request.getParameter("error"))) {
					list.add(cb.like(root.get("error").as(String.class), "%"+request.getParameter("error")+"%"));
					map.put("error", request.getParameter("error")) ;
				}
				
				//sessionID
				if(!StringUtils.isBlank(request.getParameter("reportdic"))) {
					list.add(cb.equal(root.get("reportdic").as(String.class), request.getParameter("reportdic")));
					map.put("reportdic", request.getParameter("reportdic")) ;
				}
				try {
					//创建时间区间查询
					if(!StringUtils.isBlank(request.getParameter("createdatebegin")) && request.getParameter("createdatebegin").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("createdate").as(Date.class), UKTools.dateFormate.parse(request.getParameter("createdatebegin")))) ;
						map.put("createdatebegin", request.getParameter("createdatebegin")) ;
					}
					if(!StringUtils.isBlank(request.getParameter("createdateend")) && request.getParameter("createdateend").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("createdate").as(Date.class), UKTools.dateFormate.parse(request.getParameter("createdateend")))) ;
						map.put("createdateend", request.getParameter("createdateend")) ;
					}
					
					//登陆时间或者操作时间区间查询
					if(!StringUtils.isBlank(request.getParameter("starttimebegin")) && request.getParameter("starttimebegin").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("starttime").as(Date.class), UKTools.dateFormate.parse(request.getParameter("starttimebegin")))) ;
						map.put("starttimebegin", request.getParameter("starttimebegin")) ;
					}
					if(!StringUtils.isBlank(request.getParameter("starttimeend")) && request.getParameter("starttimeend").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("starttime").as(Date.class), UKTools.dateFormate.parse(request.getParameter("starttimeend")))) ;
						map.put("starttimeend", request.getParameter("starttimeend")) ;
					}
					
					//退出时间区间查询
					if(!StringUtils.isBlank(request.getParameter("endtimebegin")) && request.getParameter("endtimebegin").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("endtime").as(Date.class), UKTools.dateFormate.parse(request.getParameter("endtimebegin")))) ;
						map.put("endtimebegin", request.getParameter("endtimebegin")) ;
					}
					if(!StringUtils.isBlank(request.getParameter("endtimeend")) && request.getParameter("endtimeend").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("endtime").as(Date.class), UKTools.dateFormate.parse(request.getParameter("endtimeend")))) ;
						map.put("endtimeend", request.getParameter("endtimeend")) ;
					}
					
					//触发预警服务时间区间查询
					if(!StringUtils.isBlank(request.getParameter("triggertimebegin")) && request.getParameter("triggertimebegin").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("triggertime").as(Date.class), UKTools.dateFormate.parse(request.getParameter("triggertimebegin")))) ;
						map.put("triggertimebegin", request.getParameter("triggertimebegin")) ;
					}
					if(!StringUtils.isBlank(request.getParameter("triggertimeend")) && request.getParameter("triggertimeend").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("triggertime").as(Date.class), UKTools.dateFormate.parse(request.getParameter("triggertimeend")))) ;
						map.put("triggertimeend", request.getParameter("triggertimeend")) ;
					}
					//请求耗时区间查询
					if(!StringUtils.isBlank(request.getParameter("querytimebegin"))){
						list.add(cb.ge(root.get("querytime").as(int.class),Integer.parseInt(request.getParameter("querytimebegin")))) ;
						map.put("querytimebegin", request.getParameter("querytimebegin")) ;
					}
					if(!StringUtils.isBlank(request.getParameter("querytimeend"))){
						list.add(cb.le(root.get("querytime").as(int.class),Integer.parseInt(request.getParameter("querytimeend")))) ;
						map.put("querytimeend", request.getParameter("querytimeend")) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				
				Predicate[] p = new Predicate[list.size()];  
				return cb.and(list.toArray(p));   
			}}, new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, new String[] { "createdate" }));
    	map.addAttribute("requestLogList", requestLogPage) ;
    	map.addAttribute("userList", userRes.findByOrgi(super.getOrgi(request))) ;
		map.addAttribute("warnlvList", UKeFuDic.getInstance().getSysDic(UKDataContext.UKEFU_SYSTEM_REQLOG_WARNLV)) ;
        return request(super.createAdminTempletResponse("/admin/system/reqlog/index"));
    }
    
    @RequestMapping("/detail")
    @Menu(type = "admin" , subtype = "reqlog" ,spadmin = true)
    public ModelAndView detail(ModelMap map , HttpServletRequest request , @Valid String id) {
    	if (!StringUtils.isBlank(id)) {
    		map.addAttribute("requestLog", requestLogRes.findByIdAndOrgi(id, super.getOrgi(request))) ;
		}
    	map.addAttribute("warnlvList", UKeFuDic.getInstance().getSysDic(UKDataContext.UKEFU_SYSTEM_REQLOG_WARNLV)) ;
        return request(super.createRequestPageTempletResponse("/admin/system/reqlog/detail"));
    }
    
    
    /**
 	 * 	日志概览
     * @param map
     * @param request
     * @return
     * @throws ParseException 
     */
    @RequestMapping("/view")
    @Menu(type = "admin" , subtype = "logview" ,spadmin = true)
    public ModelAndView view(final ModelMap map ,final HttpServletRequest request) throws ParseException {
    	this.desktop(map, request) ;
		return request(super.createAdminTempletResponse("/admin/system/reqlog/view"));
    }
    
    @RequestMapping("/desktop")
    @Menu(type = "admin" , subtype = "logview" ,spadmin = true)
    public ModelAndView desktop(final ModelMap map , final HttpServletRequest request ) throws ParseException {
//    	final String orgi = super.getOrgi(request) ;
    	Date beginDate,endDate = null;
    	if (!StringUtils.isBlank(request.getParameter("reflashbegin"))) {
    		beginDate = UKTools.dateFormate.parse(request.getParameter("reflashbegin"));
    		map.addAttribute("reflashbegin", request.getParameter("reflashbegin"));
		}else {
			beginDate = UKTools.getLastDay(7);
		}
    	if (!StringUtils.isBlank(request.getParameter("reflashend"))) {
    		endDate = UKTools.dateFormate.parse(request.getParameter("reflashend"));
    		map.addAttribute("reflashend", request.getParameter("reflashend"));
    	}else {
    		endDate = new Date() ;
		}
    	if (beginDate != null && endDate != null) {
    		Map<String,Long> aggMap = onlineUserRes.countRequestLog(beginDate ,endDate) ;
    		Double querytimeavg = onlineUserRes.avgByQuerytimeFromRequestLog(beginDate ,endDate);//平均请求耗时
    		long allcount = aggMap.get("allcount");
    		long triggertimecount = aggMap.get("triggertimecount");//触发预警
    		long triggerwarningcount = aggMap.get("triggerwarningcount");//拦截次数
    		long usertriggercount = aggMap.get("usertriggercount");//触发预警用户
    		long userwarningcount = aggMap.get("userwarningcount");//被拦截的用户
    		long errorcount = aggMap.get("errorcount");//报错提示
    		long notusercount = aggMap.get("notusercount");//非用户操作
    		map.addAttribute("allcount", allcount);
    		map.addAttribute("triggertimecount", triggertimecount);
    		map.addAttribute("triggerwarningcount", triggerwarningcount);
    		map.addAttribute("usertriggercount", usertriggercount);
    		map.addAttribute("userwarningcount", userwarningcount);
    		map.addAttribute("errorcount", errorcount);
    		map.addAttribute("querytimeavg", querytimeavg);
    		map.addAttribute("notusercount", notusercount);
    		
    		List<Object> ipList = onlineUserRes.ipGroupbyFromRequestLog(beginDate ,endDate);
    		List<Object> useridList = onlineUserRes.useridGroupbyFromRequestLog(beginDate ,endDate);
    		map.addAttribute("ipsize", ipList.size());
    		map.addAttribute("useridsize", useridList.size());
		}
    	Page<RequestLog> requestLogPage = requestLogRes.findAll(new Specification<RequestLog>(){
			@Override
			public Predicate toPredicate(Root<RequestLog> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  

//				list.add(cb.equal(root.get("orgi").as(String.class), orgi));
				list.add(cb.isNotNull(root.get("triggertime").as(Date.class)));

				try {
					if(!StringUtils.isBlank(request.getParameter("reflashbegin")) && request.getParameter("reflashbegin").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("createdate").as(Date.class), UKTools.dateFormate.parse(request.getParameter("reflashbegin")))) ;
					}else {
						list.add(cb.greaterThanOrEqualTo(root.get("createdate").as(Date.class),UKTools.getLastDay(7))) ;
					}
					if(!StringUtils.isBlank(request.getParameter("reflashend")) && request.getParameter("reflashend").matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("createdate").as(Date.class), UKTools.dateFormate.parse(request.getParameter("reflashend")))) ;
					}else {
						list.add(cb.lessThanOrEqualTo(root.get("createdate").as(Date.class), new Date())) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				
				Predicate[] p = new Predicate[list.size()];  
				return cb.and(list.toArray(p));   
			}}, new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, new String[] { "createdate" }));
    	map.addAttribute("requestLogList", requestLogPage) ;
    	return request(super.createRequestPageTempletResponse("/admin/system/reqlog/content"));
    }
    
    public BoolQueryBuilder searchBoolQueryBuilder(BoolQueryBuilder queryBuilder , ModelMap map, HttpServletRequest request) {
    	queryBuilder.must(termQuery("orgi", this.getOrgi(request))) ;
		
		//搜索框
		if(!StringUtils.isBlank(request.getParameter("q"))) {
			String q = request.getParameter("q") ;
			q = q.replaceAll("(OR|AND|NOT|:|\\(|\\))", "") ;
			if(!StringUtils.isBlank(q)){
				queryBuilder.must(QueryBuilders.boolQuery().must(new QueryStringQueryBuilder(q).defaultOperator(Operator.AND))) ;
				map.put("q", q) ;
			}
		}
		
		//操作人
		if(!StringUtils.isBlank(request.getParameter("username"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("username", request.getParameter("username"))) ;
			map.put("username", request.getParameter("username")) ;
		}
		
		//操作人员ID
		if(!StringUtils.isBlank(request.getParameter("userid"))) {
			queryBuilder.must(termQuery("userid", request.getParameter("userid"))) ;
			map.put("userid", request.getParameter("userid")) ;
		}
		
		//操作人员的EMAIL
		if(!StringUtils.isBlank(request.getParameter("usermail"))) {
			queryBuilder.must(termQuery("usermail", request.getParameter("usermail"))) ;
			map.put("usermail", request.getParameter("usermail")) ;
		}
		
		//操作url
		if(!StringUtils.isBlank(request.getParameter("url"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("url", request.getParameter("url"))) ;
			map.put("url", request.getParameter("url")) ;
		}
		
		//传入参数
		if(!StringUtils.isBlank(request.getParameter("parameters"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("parameters", request.getParameter("parameters"))) ;
			map.put("parameters", request.getParameter("parameters")) ;
		}
		
		//报表名称
		if(!StringUtils.isBlank(request.getParameter("reportname"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("reportname", request.getParameter("reportname"))) ;
			map.put("reportname", request.getParameter("reportname")) ;
		}
		
		//日志小分类
		if(!StringUtils.isBlank(request.getParameter("detailtype"))) {
			queryBuilder.must(termQuery("detailtype", request.getParameter("detailtype"))) ;
			map.put("detailtype", request.getParameter("detailtype")) ;
		}
		
		//ip地址
		if(!StringUtils.isBlank(request.getParameter("ip"))) {
			queryBuilder.must(termQuery("ip", request.getParameter("ip"))) ;
			map.put("ipp", request.getParameter("ip")) ;
		}
		
		//hostname
		if(!StringUtils.isBlank(request.getParameter("hostname"))) {
			queryBuilder.must(termQuery("hostname", request.getParameter("hostname"))) ;
			map.put("hostnamee", request.getParameter("hostname")) ;
		}
		
		//操作状态
		if(!StringUtils.isBlank(request.getParameter("statues"))) {
			queryBuilder.must(termQuery("statues", request.getParameter("statues"))) ;
			map.put("statues", request.getParameter("statues")) ;
		}
		
		//操作状态
		if(!StringUtils.isBlank(request.getParameter("triggerwarning"))) {
			queryBuilder.must(termQuery("triggerwarning", request.getParameter("triggerwarning"))) ;
			map.put("triggerwarning", request.getParameter("triggerwarning")) ;
		}
		
		//类名
		if(!StringUtils.isBlank(request.getParameter("classname"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("classname", request.getParameter("classname"))) ;
			map.put("classname", request.getParameter("classname")) ;
		}
		
		//方法名
		if(!StringUtils.isBlank(request.getParameter("methodname"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("methodname", request.getParameter("methodname"))) ;
			map.put("methodname", request.getParameter("methodname")) ;
		}
		
		//字段名
		if(!StringUtils.isBlank(request.getParameter("filename"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("filename", request.getParameter("filename"))) ;
			map.put("filename", request.getParameter("filename")) ;
		}
		
		//行号
		if(!StringUtils.isBlank(request.getParameter("linenumber"))) {
			queryBuilder.must(termQuery("linenumber", request.getParameter("linenumber"))) ;
			map.put("linenumber", request.getParameter("linenumber")) ;
		}
		
		//线程
		if(!StringUtils.isBlank(request.getParameter("throwable"))) {
			queryBuilder.must(termQuery("throwable", request.getParameter("throwable"))) ;
			map.put("throwable", request.getParameter("throwable")) ;
		}
		
		//Menu type
		if(!StringUtils.isBlank(request.getParameter("funtype"))) {
			queryBuilder.must(termQuery("funtype", request.getParameter("funtype"))) ;
			map.put("funtype", request.getParameter("funtype")) ;
		}
		
		//Menu subtype
		if(!StringUtils.isBlank(request.getParameter("fundesc"))) {
			queryBuilder.must(termQuery("fundesc", request.getParameter("fundesc"))) ;
			map.put("fundesc", request.getParameter("fundesc")) ;
		}
		
		if(!StringUtils.isBlank(request.getParameter("reportdic"))) {
			queryBuilder.must(termQuery("reportdic", request.getParameter("reportdic"))) ;
			map.put("reportdic", request.getParameter("reportdic")) ;
		}
		
		if(!StringUtils.isBlank(request.getParameter("error"))) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("error", request.getParameter("error"))) ;
			map.put("error", request.getParameter("error")) ;
		}
    	
		RangeQueryBuilder rangeQuery = null ;
		//创建时间区间查询
		if(!StringUtils.isBlank(request.getParameter("createdatebegin")) || !StringUtils.isBlank(request.getParameter("createdateend"))){
			if(!StringUtils.isBlank(request.getParameter("createdatebegin"))) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("createdate").from(UKTools.dateFormate.parse(request.getParameter("createdatebegin")).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(request.getParameter("createdateend")) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("createdate").to(UKTools.dateFormate.parse(request.getParameter("createdateend")).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(request.getParameter("createdateend")).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			map.put("createdatebegin", request.getParameter("createdatebegin")) ;
			map.put("createdateend", request.getParameter("createdateend")) ;
		}
		
		
		//登陆时间或者操作时间区间查询
		if(!StringUtils.isBlank(request.getParameter("starttimebegin")) || !StringUtils.isBlank(request.getParameter("starttimeend"))){
			if(!StringUtils.isBlank(request.getParameter("starttimebegin"))) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("starttime").from(UKTools.dateFormate.parse(request.getParameter("starttimebegin")).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(request.getParameter("starttimeend")) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("starttime").to(UKTools.dateFormate.parse(request.getParameter("starttimeend")).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(request.getParameter("starttimeend")).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			map.put("starttimebegin", request.getParameter("starttimebegin")) ;
			map.put("starttimeend", request.getParameter("starttimeend")) ;
		}
		
		//退出时间区间查询
		if(!StringUtils.isBlank(request.getParameter("endtimebegin")) || !StringUtils.isBlank(request.getParameter("endtimeend"))){
			if(!StringUtils.isBlank(request.getParameter("endtimebegin"))) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("endtime").from(UKTools.dateFormate.parse(request.getParameter("endtimebegin")).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(request.getParameter("endtimeend")) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("endtime").to(UKTools.dateFormate.parse(request.getParameter("endtimeend")).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(request.getParameter("endtimeend")).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
			}
			map.put("endtimebegin", request.getParameter("endtimebegin")) ;
			map.put("endtimeend", request.getParameter("endtimeend")) ;
		}
		
		//触发预警服务时间区间查询
		if(!StringUtils.isBlank(request.getParameter("triggertimebegin")) || !StringUtils.isBlank(request.getParameter("triggertimeend"))){
			if(!StringUtils.isBlank(request.getParameter("triggertimebegin"))) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("triggertime").from(UKTools.dateFormate.parse(request.getParameter("triggertimebegin")).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(request.getParameter("triggertimeend")) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("triggertime").to(UKTools.dateFormate.parse(request.getParameter("triggertimeend")).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(request.getParameter("triggertimeend")).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
			}
			map.put("triggertimebegin", request.getParameter("triggertimebegin")) ;
			map.put("triggertimeend", request.getParameter("triggertimeend")) ;
		}
		
		//请求耗时区间查询
		if(!StringUtils.isBlank(request.getParameter("querytimebegin")) || !StringUtils.isBlank(request.getParameter("querytimeend"))){
			if(!StringUtils.isBlank(request.getParameter("querytimebegin"))) {
				rangeQuery = QueryBuilders.rangeQuery("querytime").from(request.getParameter("querytimebegin")) ;
			}
			if(!StringUtils.isBlank(request.getParameter("querytimeend")) ) {
				if(rangeQuery == null) {
					rangeQuery = QueryBuilders.rangeQuery("querytime").to(request.getParameter("querytimeend")) ;
				}else {
					rangeQuery.to(request.getParameter("querytimeend")) ;
				}
			}
			map.put("querytimebegin", request.getParameter("querytimebegin")) ;
			map.put("querytimeend", request.getParameter("querytimeend")) ;
		}
		if(rangeQuery!=null) {
			queryBuilder.must(rangeQuery) ;
		}
    	return queryBuilder ;
    }
    
}