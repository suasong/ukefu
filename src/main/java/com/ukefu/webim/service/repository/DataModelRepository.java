package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.DataModel;

public abstract interface DataModelRepository extends JpaRepository<DataModel, String> {
	
	public abstract DataModel findByIdAndOrgi(String id, String orgi);

	public abstract List<DataModel> findByProidAndOrgi(String proid , String orgi ) ;
	
	public abstract Page<DataModel> findByProidAndOrgi(String proid , String orgi ,Pageable paramPageable) ;
	
	public abstract Page<DataModel> findAll(Specification<DataModel> spec, Pageable page) ;
	
//	public abstract List<DataModel> findByViewtypeAndProidAndOrgi(String viewtype ,String proid, String orgi ) ;
	
	public abstract List<DataModel> findByViewtypeAndProidAndOrgiOrderByCreatetimeDesc(String viewtype ,String proid, String orgi ) ;
	
	public abstract List<DataModel> findByProidAndOrgiOrderBySortindexAsc(String proid , String orgi ) ;
	
	public abstract List<DataModel> findByViewtypeAndProidAndParentidAndOrgi(String viewtype ,String proid,String parentid, String orgi ) ;
	
	public abstract List<DataModel> findByParentidAndOrgi(String parentid, String orgi ) ;
	
	public abstract DataModel findByCodeAndOrgi(String code, String orgi);
	
	public abstract List<DataModel> findByParentidAndOrgiOrderBySortindexAsc(String parentid, String orgi ) ;
}
