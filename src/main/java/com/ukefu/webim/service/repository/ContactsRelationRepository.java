package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.ContactsRelation;

public abstract interface ContactsRelationRepository extends JpaRepository<ContactsRelation, String>
{
  
  public abstract List<ContactsRelation> findByContactsidAndRelateidAndOrgi(String contactsid, String relateid,String orgi);
  
  public abstract long countByContactsidAndRelateidAndOrgi(String contactsid, String relateid,String orgi);
  
  public abstract List<ContactsRelation> findByContactsidAndOrgi(String contactsid,String orgi);
  
  public abstract ContactsRelation findByIdAndOrgi(String id,String orgi);
  
}
