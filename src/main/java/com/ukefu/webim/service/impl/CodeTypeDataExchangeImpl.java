package com.ukefu.webim.service.impl;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ukefu.util.extra.DataExchangeInterface; 
import com.ukefu.webim.service.repository.TagRepository; 
import com.ukefu.webim.web.model.Tag;

@Service("codetype")
public class CodeTypeDataExchangeImpl implements DataExchangeInterface{
	@Autowired
	private TagRepository tagRepository ;
	
	public String getDataByIdAndOrgi(String id, String orgi){
		return tagRepository.findByOrgiAndId(orgi,id).getTag();
	}

	@Override
	public List<Tag> getListDataByIdAndOrgi(String id , String creater, String orgi) {
		return null;
	}
	
	public void process(Object data , String orgi) {
		
	}
}
