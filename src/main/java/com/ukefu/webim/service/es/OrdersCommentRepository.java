package com.ukefu.webim.service.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.OrdersComment;

public interface OrdersCommentRepository extends  ElasticsearchRepository<OrdersComment, String> , OrdersCommentEsCommonRepository{
}
