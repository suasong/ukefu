CREATE TABLE `uk_crm_datamodel` (
  `ID` varchar(32) NOT NULL DEFAULT '' COMMENT 'ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `REPORTTYPE` varchar(32) DEFAULT NULL COMMENT '类型',
  `TITLE` varchar(255) DEFAULT NULL COMMENT '标题',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `OBJECTCOUNT` int(11) DEFAULT NULL COMMENT '对象数量',
  `DICID` varchar(32) DEFAULT NULL COMMENT '菜单ID',
  `CREATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `DESCRIPTION` longtext COMMENT '描述',
  `HTML` longtext COMMENT '代码',
  `REPORTPACKAGE` varchar(255) DEFAULT NULL COMMENT '目录名称',
  `USEACL` varchar(32) DEFAULT NULL COMMENT '授权信息',
  `status` varchar(32) DEFAULT NULL COMMENT '状态',
  `rolename` text COMMENT '角色名称',
  `userid` text COMMENT '创建用户ID',
  `blacklist` text COMMENT '黑名单',
  `REPORTCONTENT` longtext COMMENT '对象内容',
  `reportmodel` varchar(32) DEFAULT NULL COMMENT '模型',
  `updatetime` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `reportversion` int(11) DEFAULT NULL COMMENT '版本号',
  `publishedtype` varchar(32) DEFAULT NULL COMMENT '发布状态',
  `tabtype` varchar(32) DEFAULT NULL COMMENT '元素类型',
  `USERNAME` varchar(32) DEFAULT NULL COMMENT '用户名',
  `USEREMAIL` varchar(255) DEFAULT NULL COMMENT '用户邮件信息',
  `CACHE` smallint(6) DEFAULT NULL COMMENT '是否启用数据缓存',
  `EXTPARAM` varchar(255) DEFAULT NULL COMMENT '扩展数据',
  `TARGETREPORT` varchar(32) DEFAULT NULL COMMENT '模板对象',
  `VIEWTYPE` varchar(32) DEFAULT NULL COMMENT '对象类型',
  `ACTIONTYPE` varchar(32) DEFAULT NULL COMMENT '操作类型',
  `STYLE` varchar(32) DEFAULT NULL COMMENT '样式信息',
  `ICON` varchar(32) DEFAULT NULL COMMENT '图标',
  `TBID` varchar(32) DEFAULT NULL COMMENT '元数据ID',
  `TBNAME` varchar(255) DEFAULT NULL COMMENT '元数据名称',
  `ACTION` varchar(32) DEFAULT NULL COMMENT '动作',
  `BTNTYPE` varchar(32) DEFAULT NULL COMMENT '按钮类型',
  `DESIGN` varchar(32) DEFAULT NULL COMMENT '显示值',
  `SORTINDEX` int(11) DEFAULT NULL COMMENT '排序序号',
  `PROID` varchar(32) DEFAULT NULL COMMENT '产品ID',
  `AUTHCODE` varchar(50) DEFAULT NULL COMMENT '权限代码',
  `AUTHITEM` varchar(50) DEFAULT NULL COMMENT '授权时间',
  `DSTYPE` varchar(32) DEFAULT NULL COMMENT '数据源类型',
  `RTFEDIT` tinyint(4) DEFAULT NULL COMMENT '是否允许编辑文本',
  `CODEEDIT` tinyint(4) DEFAULT NULL COMMENT '是否允许编辑代码',
  `LINKTYPE` varchar(32) DEFAULT NULL COMMENT '链接类型',
  `LINKURL` varchar(255) DEFAULT NULL COMMENT '链接跳转URL',
  `PARAMS` varchar(255) DEFAULT NULL COMMENT '扩展参数',
  `LAYOUTTYPE` varchar(32) DEFAULT NULL COMMENT '布局类型',
  `upload` tinyint(4) DEFAULT NULL COMMENT '是否启用上传',
  `hasform` tinyint(4) DEFAULT NULL COMMENT '是否有表单',
  `formtype` varchar(32) DEFAULT NULL COMMENT '表单类型',
  `submiturl` varchar(255) DEFAULT NULL COMMENT '提交跳转的URL',
  `submitpage` varchar(255) DEFAULT NULL COMMENT '跳转页面',
  `submitlink` varchar(255) DEFAULT NULL COMMENT '提交URL',
  `reseturl` varchar(255) DEFAULT NULL COMMENT '重置后跳转URL',
  `resetpage` varchar(255) DEFAULT NULL COMMENT '重置后跳转地址',
  `resetlink` varchar(255) DEFAULT NULL COMMENT '重置链接',
  `resetbtn` tinyint(4) DEFAULT NULL COMMENT '是否启用重置按钮',
  `submitbtn` tinyint(4) DEFAULT NULL COMMENT '是否启用提交按钮',
  `resetpagerpt` varchar(32) DEFAULT NULL COMMENT '重置页面',
  `submitpagerpt` varchar(255) DEFAULT NULL COMMENT '提交页面',
  `layoutleft` tinyint(4) DEFAULT NULL COMMENT '启用左侧分栏',
  `layoutright` tinyint(4) DEFAULT NULL COMMENT '启用右侧分栏',
  `layoutcenter` tinyint(4) DEFAULT NULL COMMENT '启用中间分栏',
  `leftscroll` tinyint(4) DEFAULT NULL COMMENT '启用左侧滚动',
  `rightscroll` tinyint(4) DEFAULT NULL COMMENT '启用右侧滚动',
  `centerscroll` tinyint(4) DEFAULT NULL COMMENT '启用中间滚动',
  `leftwidth` varchar(32) DEFAULT NULL COMMENT '左侧间隔',
  `rightwidth` varchar(32) DEFAULT NULL COMMENT '右侧间隔',
  `centerheight` varchar(32) DEFAULT NULL COMMENT '中间间隔',
  `mediaagent` tinyint(4) DEFAULT NULL COMMENT '坐席',
  `hisnav` tinyint(4) DEFAULT NULL COMMENT '是否有导航',
  `submittype` varchar(32) DEFAULT NULL COMMENT '提交类型',
  `submitpos` varchar(32) DEFAULT NULL COMMENT '表单提交后更新页面位置',
  `MGROUP` tinyint(4) DEFAULT '0' COMMENT '是否启用分组',
  `groupname` varchar(100) DEFAULT NULL COMMENT '分组名称',
  `groupicon` varchar(100) DEFAULT NULL COMMENT '分组图标',
  `groupcolor` varchar(100) DEFAULT NULL COMMENT '分组颜色',
  `workflow` tinyint(4) DEFAULT NULL COMMENT '启用流程',
  `flowtype` varchar(50) DEFAULT NULL COMMENT '流程类型',
  `successtip` varchar(255) DEFAULT NULL COMMENT '成功提示',
  `errortip` varchar(255) DEFAULT NULL COMMENT '失败提示',
  `fullscreen` tinyint(4) DEFAULT NULL COMMENT '全屏',
  `autorefresh` tinyint(4) DEFAULT NULL COMMENT '自动刷新',
  `refreshtime` int(11) DEFAULT NULL COMMENT '刷新间隔',
  `autoscroll` tinyint(4) DEFAULT NULL COMMENT '自动滚动',
  `scrollspeed` varchar(32) DEFAULT NULL COMMENT '滚动速度',
  `onlytab` tinyint(4) DEFAULT NULL COMMENT '大屏显示的时候是否只显示 页签',
  `rotationspeed` int(11) DEFAULT NULL COMMENT '大屏显示的时候轮播速度',
  `accesshis` tinyint(4) DEFAULT NULL COMMENT '是否记录 访问历史',
  `searchhis` tinyint(4) DEFAULT NULL COMMENT '是否记录搜索历史',
  `CODE` varchar(100) DEFAULT NULL COMMENT '代码',
  `duplicate` tinyint(4) DEFAULT NULL COMMENT '是否允许重复数据',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `uk_crm_dataproduct` (
  `ID` varchar(32) NOT NULL COMMENT 'ID',
  `NAME` varchar(32) DEFAULT NULL COMMENT '名称',
  `TITLE` varchar(32) DEFAULT NULL COMMENT '标题',
  `CODE` varchar(32) DEFAULT NULL COMMENT ' 代码',
  `PARENTID` varchar(32) DEFAULT NULL COMMENT '上级ID',
  `TYPE` varchar(32) DEFAULT NULL COMMENT '类型',
  `MEMO` varchar(255) DEFAULT NULL COMMENT '备注',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `STATUS` varchar(32) DEFAULT NULL COMMENT '状态',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `CREATER` varchar(255) DEFAULT NULL COMMENT '创建人',
  `PUBLISHEDTYPE` varchar(32) DEFAULT NULL COMMENT '发布类型',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '描述信息',
  `TABTYPE` varchar(32) DEFAULT NULL COMMENT '产品类型',
  `DSTYPE` varchar(32) DEFAULT NULL COMMENT '显示类型',
  `DSTEMPLET` varchar(255) DEFAULT NULL COMMENT '显示模板',
  `SORTINDEX` int(11) DEFAULT NULL COMMENT '排序序号',
  `DICTYPE` varchar(32) DEFAULT NULL COMMENT '目录类型',
  `ICONCLASS` varchar(100) DEFAULT NULL COMMENT '图标样式',
  `CSSSTYLE` varchar(255) DEFAULT NULL COMMENT '显示样式',
  `AUTHCODE` varchar(100) DEFAULT NULL COMMENT '授权代码',
  `DEFAULTMENU` tinyint(4) DEFAULT NULL COMMENT '默认产品',
  `DATAID` varchar(32) DEFAULT NULL COMMENT '外部数据ID',
  `DICICON` varchar(32) DEFAULT NULL COMMENT ' 目录图标',
  `CURICON` varchar(32) DEFAULT NULL COMMENT '选中图标',
  `BGCOLOR` varchar(32) DEFAULT NULL COMMENT '背景颜色',
  `CURBGCOLOR` varchar(32) DEFAULT NULL COMMENT '选中的背景颜色',
  `MENUPOS` varchar(32) DEFAULT NULL COMMENT '菜单位置',
  `DISTITLE` varchar(100) DEFAULT NULL COMMENT '显示标题',
  `NAVMENU` tinyint(4) DEFAULT '0' COMMENT '显示菜单',
  `QUICKMENU` tinyint(4) DEFAULT '0' COMMENT '是否显示在快捷菜单',
  UNIQUE KEY `SQL121227155530400` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE uk_noticemsg ADD invalidtime datetime DEFAULT NULL COMMENT '失效时间';

ALTER TABLE uk_consult_invite ADD lvtipmsg text DEFAULT NULL COMMENT '无坐席在线的提示消息';
ALTER TABLE uk_crm_dataproduct ADD duplicate tinyint(4) DEFAULT 0 COMMENT '是否允许重复数据';
ALTER TABLE uk_crm_dataproduct ADD design tinyint(4) DEFAULT 0 COMMENT '布局';

DROP INDEX index_1 ON uk_callcenter_event;
CREATE INDEX index_1 ON uk_callcenter_event  (`ORGI`,`DISCALLER`,`DISCALLED`,`MISSCALL`,`DURATION`,`RINGDURATION`,`calltype`,`servicestatus`,`direction`,`userid`,`organ`,`createtime`,`starttime`,`nameid`) USING BTREE;




ALTER TABLE uk_consult_invite ADD filterscript tinyint(4) DEFAULT 0 COMMENT '禁止访客端发送HTML内容';
ALTER TABLE uk_consult_invite ADD filteragentscript tinyint(4) DEFAULT 0 COMMENT '禁止坐席端发送HTML内容';

ALTER TABLE uk_consult_invite ADD filteragentscript tinyint(4) DEFAULT 0 COMMENT '禁止坐席端发送HTML内容';

ALTER TABLE uk_consult_invite ADD filteragentscript tinyint(4) DEFAULT 0 COMMENT '禁止坐席端发送HTML内容';

ALTER TABLE uk_agentuser ADD filterscript int DEFAULT 0 COMMENT '访客段脚本过滤次数';
ALTER TABLE uk_agentuser ADD filteragentscript int DEFAULT 0 COMMENT '座席端脚本过滤次数';

ALTER TABLE uk_agentuser ADD sensitiveword int DEFAULT 0 COMMENT '访客端敏感词触发次数';
ALTER TABLE uk_agentuser ADD sensitivewordagent int DEFAULT 0 COMMENT '坐席端敏感词触发次数';

ALTER TABLE uk_agentuser ADD msgtimeout int DEFAULT 0 COMMENT '访客端消息超时次数';
ALTER TABLE uk_agentuser ADD msgtimeoutagent int DEFAULT 0 COMMENT '坐席端消息敏感词触发次数';

ALTER TABLE uk_agentuser ADD sessiontimeout int DEFAULT 0 COMMENT '会话超时次数';


ALTER TABLE uk_agentservice ADD filterscript int DEFAULT 0 COMMENT '访客段脚本过滤次数';
ALTER TABLE uk_agentservice ADD filteragentscript int DEFAULT 0 COMMENT '座席端脚本过滤次数';

ALTER TABLE uk_agentservice ADD sensitiveword int DEFAULT 0 COMMENT '访客端敏感词触发次数';
ALTER TABLE uk_agentservice ADD sensitivewordagent int DEFAULT 0 COMMENT '坐席端敏感词触发次数';

ALTER TABLE uk_agentservice ADD msgtimeout int DEFAULT 0 COMMENT '访客端消息超时次数';
ALTER TABLE uk_agentservice ADD msgtimeoutagent int DEFAULT 0 COMMENT '坐席端消息敏感词触发次数';

ALTER TABLE uk_agentservice ADD sessiontimeout int DEFAULT 0 COMMENT '会话超时次数';


ALTER TABLE uk_chat_message ADD filterscript tinyint(4) DEFAULT 0 COMMENT '触发了HTML代码过滤';


ALTER TABLE uk_callcenter_extention ADD greetlong VARCHAR(100) DEFAULT NULL COMMENT '欢迎提示语音';
ALTER TABLE uk_callcenter_extention ADD greetshort VARCHAR(100) DEFAULT NULL COMMENT '欢迎提示短语音';
ALTER TABLE uk_callcenter_extention ADD invalidsound VARCHAR(100) DEFAULT NULL COMMENT '无效输入提示语音';
ALTER TABLE uk_callcenter_extention ADD exitsound VARCHAR(100) DEFAULT NULL COMMENT '离开语音';
ALTER TABLE uk_callcenter_extention ADD confirmmacro VARCHAR(50) DEFAULT NULL COMMENT '确认宏指令';
ALTER TABLE uk_callcenter_extention ADD confirmkey VARCHAR(50) DEFAULT NULL COMMENT '确认按键';
ALTER TABLE uk_callcenter_extention ADD ttsengine VARCHAR(20) DEFAULT NULL COMMENT 'TTS引擎';
ALTER TABLE uk_callcenter_extention ADD ttsvoice VARCHAR(50) DEFAULT NULL COMMENT 'TTS语音';
ALTER TABLE uk_callcenter_extention ADD confirmattempts VARCHAR(50) DEFAULT NULL COMMENT '确认提示消息';
ALTER TABLE uk_callcenter_extention ADD timeout int DEFAULT 0 COMMENT '超时时间';

ALTER TABLE uk_callcenter_extention ADD interdigittimeout int DEFAULT 0 COMMENT '呼叫等待超时';
ALTER TABLE uk_callcenter_extention ADD maxfailures int DEFAULT 0 COMMENT '最大失败次数';

ALTER TABLE uk_callcenter_extention ADD maxtimeouts int DEFAULT 0 COMMENT '最大超时次数';

ALTER TABLE uk_callcenter_extention ADD digitlen int DEFAULT 0 COMMENT '数字按键长度';
ALTER TABLE uk_callcenter_extention ADD action VARCHAR(50) DEFAULT NULL COMMENT '指令';
ALTER TABLE uk_callcenter_extention ADD digits VARCHAR(50) DEFAULT NULL COMMENT '拨号键';
ALTER TABLE uk_callcenter_extention ADD param VARCHAR(255) DEFAULT NULL COMMENT '参数';


ALTER TABLE uk_consult_invite ADD enableinvite int DEFAULT 0 COMMENT '显示默认咨询快捷提示';
ALTER TABLE uk_consult_invite ADD invitetiptitle varchar(50) DEFAULT NULL COMMENT '邀请咨询组件提示标题' ;
ALTER TABLE uk_consult_invite ADD invitetip text DEFAULT NULL COMMENT '邀请咨询提示HTML' ;
ALTER TABLE uk_consult_invite ADD invitetipdelay int DEFAULT 0 COMMENT '延时显示' ;

ALTER TABLE uk_consult_invite ADD enablecallback int DEFAULT 0 COMMENT '显示回呼组件';
ALTER TABLE uk_consult_invite ADD callbacknum  VARCHAR(50) DEFAULT NULL COMMENT '回呼号码' ;	
ALTER TABLE uk_consult_invite ADD callbacktxt  VARCHAR(50) DEFAULT NULL COMMENT '回呼提示文字' ;	
ALTER TABLE uk_consult_invite ADD callbackquicktiptitle varchar(50) DEFAULT NULL COMMENT '回呼提示标题' ;
ALTER TABLE uk_consult_invite ADD callbackquicktip text DEFAULT NULL COMMENT '回呼提示HTML' ;
ALTER TABLE uk_consult_invite ADD callbackurl  VARCHAR(255) DEFAULT NULL COMMENT '回呼点击后跳转' ;
ALTER TABLE uk_consult_invite ADD callbackicon  VARCHAR(50) DEFAULT NULL COMMENT '回呼图标' ;	
ALTER TABLE uk_consult_invite ADD callbackcolor  VARCHAR(50) DEFAULT NULL COMMENT '回呼背景颜色' ;	

ALTER TABLE uk_consult_invite ADD enabledemo int DEFAULT 0 COMMENT '显示预约演示组件';
ALTER TABLE uk_consult_invite ADD demourl  VARCHAR(255) DEFAULT NULL COMMENT '预约演示URL' ;	
ALTER TABLE uk_consult_invite ADD demotxt  VARCHAR(50) DEFAULT NULL COMMENT '预约演示文字' ;	
ALTER TABLE uk_consult_invite ADD demoquicktiptitle varchar(50) DEFAULT NULL COMMENT '预约演示提示标题' ; 	
ALTER TABLE uk_consult_invite ADD demoquicktip text DEFAULT NULL COMMENT '预约演示提示HTML' ; 	
ALTER TABLE uk_consult_invite ADD demoicon  VARCHAR(50) DEFAULT NULL COMMENT '预约演示图标' ;	
ALTER TABLE uk_consult_invite ADD democolor  VARCHAR(50) DEFAULT NULL COMMENT '预约演示背景颜色' ;	


ALTER TABLE uk_consult_invite ADD enablesns int DEFAULT 0 COMMENT '显示微信公众号组件';
ALTER TABLE uk_consult_invite ADD snsurl  VARCHAR(255) DEFAULT NULL COMMENT '公众号跳转URL' ;		
ALTER TABLE uk_consult_invite ADD snstxt  VARCHAR(50) DEFAULT NULL COMMENT '公众号提示文字' ;		
ALTER TABLE uk_consult_invite ADD snstiptitle varchar(50) DEFAULT NULL COMMENT '公众号提示标题' ; 	
ALTER TABLE uk_consult_invite ADD snstip text DEFAULT NULL COMMENT '公众号提示HTML' ; 	
ALTER TABLE uk_consult_invite ADD snsicon  VARCHAR(50) DEFAULT NULL COMMENT '公众号图标' ;	
ALTER TABLE uk_consult_invite ADD snsqrcode  VARCHAR(50) DEFAULT NULL COMMENT '公众号二维码' ;	
ALTER TABLE uk_consult_invite ADD snscolor  VARCHAR(50) DEFAULT NULL COMMENT '公众号背景颜色' ;	


ALTER TABLE uk_consult_invite ADD enableothermodel int DEFAULT 0 COMMENT '显示其他组件';
ALTER TABLE uk_consult_invite ADD othermodelurl  VARCHAR(255) DEFAULT NULL COMMENT '其他组件URL' ;	
ALTER TABLE uk_consult_invite ADD othermodeltxt  VARCHAR(50) DEFAULT NULL COMMENT '其他组件文本' ;	
ALTER TABLE uk_consult_invite ADD othermodeltiptitle varchar(50) DEFAULT NULL COMMENT '其他组件提示标题' ;	
ALTER TABLE uk_consult_invite ADD othermodeltip text DEFAULT NULL COMMENT '其他组件提示HTML' ;	
ALTER TABLE uk_consult_invite ADD othermodelicon  VARCHAR(50) DEFAULT NULL COMMENT '其他组件图标' ;	
ALTER TABLE uk_consult_invite ADD othermodelcolor  VARCHAR(50) DEFAULT NULL COMMENT '其他组件背景颜色' ;

ALTER TABLE uk_crm_datamodel ADD targetid varchar(32) DEFAULT NULL COMMENT '目标id';
ALTER TABLE uk_crm_dataproduct ADD TBID varchar(32) DEFAULT NULL COMMENT '元数据ID';
ALTER TABLE uk_crm_dataproduct ADD menuid varchar(32) DEFAULT NULL COMMENT '菜单ID';
ALTER TABLE uk_crm_dataproduct ADD scheme varchar(255) DEFAULT NULL COMMENT '方案';

INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('40281381692dde0901692de349610012', '链接', 'pub', 'com.dic.href', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-2-27 15:37:44', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('40281381692dde0901692de2f45b000b', '按钮', 'pub', 'com.dic.button', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-2-27 15:37:22', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL);

ALTER TABLE uk_tableproperties ADD freesearch tinyint(4) DEFAULT 0 COMMENT '搜索方式（0:不支持，1：快速搜索，2：高级搜索）';

ALTER TABLE uk_crm_datamodel ADD targetid varchar(32) DEFAULT NULL COMMENT '目标id';
ALTER TABLE uk_crm_dataproduct ADD TBID varchar(32) DEFAULT NULL COMMENT '元数据ID';
ALTER TABLE uk_crm_dataproduct ADD menuid varchar(32) DEFAULT NULL COMMENT '菜单ID';
ALTER TABLE uk_crm_dataproduct ADD scheme varchar(255) DEFAULT NULL COMMENT '方案';

CREATE TABLE `uk_crm_scheme` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `code` varchar(255) DEFAULT NULL COMMENT '代码',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` date DEFAULT NULL COMMENT '创建时间',
  `updatetime` date DEFAULT NULL COMMENT '更新时间',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='crm-方案表';

ALTER TABLE uk_crm_dataproduct ADD datamodelid varchar(32) DEFAULT NULL COMMENT '默认页面布局id';
ALTER TABLE uk_crm_dataproduct ADD skiptype varchar(32) DEFAULT NULL COMMENT '跳转方式';
ALTER TABLE uk_crm_dataproduct ADD skipurl varchar(32) DEFAULT NULL COMMENT '跳转url';
ALTER TABLE uk_crm_dataproduct ADD defaulshow tinyint(4) DEFAULT '0' COMMENT '是否默认显示';
ALTER TABLE uk_crm_dataproduct ADD iconcolor varchar(100) DEFAULT NULL COMMENT '图标颜色';

ALTER TABLE uk_crm_dataproduct ADD allowfilter tinyint(4) DEFAULT '0' COMMENT '是否允许筛选列';
ALTER TABLE uk_crm_dataproduct ADD allowprint tinyint(4) DEFAULT '0' COMMENT '是否允许打印';
ALTER TABLE uk_crm_dataproduct ADD allowexports tinyint(4) DEFAULT '0' COMMENT '是否允许导出';

ALTER TABLE uk_callcenter_ivr ADD playsound VARCHAR(100) DEFAULT NULL COMMENT '播放语音';

ALTER TABLE uk_agentservice ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';
ALTER TABLE uk_agentuser ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';


ALTER TABLE uk_callcenter_extention ADD autoanswer varchar(20) DEFAULT NULL COMMENT '来电自动接听';
ALTER TABLE uk_agentuser ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';
ALTER TABLE uk_agentuser ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';

ALTER TABLE uk_que_result ADD answerlevel VARCHAR(50) DEFAULT NULL COMMENT '评级 a b c d';

ALTER TABLE uk_crm_datamodel ADD parentid varchar(32) DEFAULT NULL COMMENT '父级id';


ALTER TABLE uk_callcenter_event ADD autoanswer tinyint DEFAULT 0 COMMENT '是否自动接听';

ALTER TABLE uk_agentuser ADD endby varchar(20) DEFAULT NULL COMMENT '挂断方';
ALTER TABLE uk_callcenter_event ADD autoanswer tinyint DEFAULT 0 COMMENT '是否自动接听';


ALTER TABLE uk_noticemsg ADD sqlurl varchar(255) DEFAULT NULL COMMENT '下载sql地址';
ALTER TABLE uk_noticemsg ADD version varchar(255) DEFAULT NULL COMMENT '版本号';
ALTER TABLE uk_systemconfig ADD version varchar(255) DEFAULT NULL COMMENT '版本号';

ALTER TABLE uk_noticemsg ADD jarurldownload TINYINT(4) DEFAULT 0 COMMENT 'jar包是否下载成功';
ALTER TABLE uk_noticemsg ADD sqlurldownload TINYINT(4) DEFAULT 0 COMMENT 'sql是否下载成功';
ALTER TABLE uk_noticemsg ADD udpatestatus TINYINT(4) DEFAULT 0 COMMENT '更新状态 0未更新 1已执行更新';

ALTER TABLE uk_noticemsg ADD rollbacksqlurl varchar(255) DEFAULT NULL COMMENT '下载回滚sql地址';

ALTER TABLE uk_noticemsg ADD rollbacksqlurldownload TINYINT(4) DEFAULT 0 COMMENT '回滚sql是否下载成功';

ALTER TABLE uk_system_updatecon ADD confirm TINYINT(4) DEFAULT NULL COMMENT '是否确认更新';
ALTER TABLE uk_system_updatecon ADD jarurl varchar(255) DEFAULT NULL COMMENT '下载sql地址';
ALTER TABLE uk_system_updatecon ADD sqlurl varchar(255) DEFAULT NULL COMMENT '下载sql地址';
ALTER TABLE uk_system_updatecon ADD oldversion varchar(255) DEFAULT NULL COMMENT '版本号';
ALTER TABLE uk_system_updatecon ADD version varchar(255) DEFAULT NULL COMMENT '版本号';
ALTER TABLE uk_system_updatecon ADD versiondesc varchar(500) DEFAULT NULL COMMENT '版本更新说明';
ALTER TABLE uk_system_updatecon ADD jarurldownload TINYINT(4) DEFAULT 0 COMMENT 'jar包是否下载成功';
ALTER TABLE uk_system_updatecon ADD sqlurldownload TINYINT(4) DEFAULT 0 COMMENT 'sql是否下载成功';
ALTER TABLE uk_system_updatecon ADD udpatestatus TINYINT(4) DEFAULT 0 COMMENT '更新状态 0未更新 1已执行更新';

ALTER TABLE uk_system_updatecon ADD rollbacksqlurl varchar(255) DEFAULT NULL COMMENT '下载回滚sql地址';

ALTER TABLE uk_system_updatecon ADD rollbacksqlurldownload TINYINT(4) DEFAULT 0 COMMENT '回滚sql是否下载成功';

ALTER TABLE uk_systemconfig ADD appid varchar(255) DEFAULT NULL COMMENT '客户端id';



ALTER TABLE uk_callcenter_event ADD answersip text DEFAULT NULL COMMENT '应答事件SIP';
ALTER TABLE uk_callcenter_event ADD hangupsip text DEFAULT NULL COMMENT '挂断事件SIP';

ALTER TABLE uk_agentservice ADD useful tinyint(4) DEFAULT '0' COMMENT '是否解决（1 解决），该会话是否解决了访客问题，一般用在机器人会话';

ALTER TABLE uk_chat_message ADD puremsg text DEFAULT NULL COMMENT '纯文本消息';

ALTER TABLE uk_xiaoe_config ADD sysmsgtransagent tinyint(4) DEFAULT 0 COMMENT '未命中转人工按钮';

ALTER TABLE uk_xiaoe_config ADD sysmsgtransagentmsg varchar(255) DEFAULT NULL COMMENT '未命中转人工提示文本';


ALTER TABLE uk_consult_invite ADD staticagent tinyint(4) DEFAULT 0 COMMENT '是否启用无感知转人工';
ALTER TABLE uk_consult_invite ADD notmatchnum int(11) DEFAULT 0 COMMENT '无命中阀值';
ALTER TABLE uk_consult_invite ADD asknum int(11) DEFAULT 0 COMMENT '对话次数阀值';
ALTER TABLE uk_consult_invite ADD topictransnum int(11) DEFAULT 0 COMMENT '话题转换次数阀值';
ALTER TABLE uk_consult_invite ADD topicmatch tinyint(4) DEFAULT 0 COMMENT '启用知识库/多轮对话/场景直接转人工';

ALTER TABLE uk_chat_message ADD staticagent tinyint(4) DEFAULT 0 COMMENT '触发无感知转人工';

ALTER TABLE uk_xiaoe_scene ADD staticagent tinyint(4) DEFAULT 0 COMMENT '触发无感知转人工';
ALTER TABLE uk_xiaoe_topic ADD staticagent tinyint(4) DEFAULT 0 COMMENT '触发无感知转人工';

ALTER TABLE uk_xiaoe_scene ADD bussop tinyint(4) DEFAULT 0 COMMENT '触发业务操作';
ALTER TABLE uk_xiaoe_scene ADD busslist text DEFAULT NULL COMMENT '触发业务列表';

ALTER TABLE uk_xiaoe_topic ADD bussop tinyint(4) DEFAULT 0 COMMENT '触发业务操作';
ALTER TABLE uk_xiaoe_topic ADD busslist text DEFAULT NULL COMMENT '触发业务列表';

ALTER TABLE uk_xiaoe_config ADD bussop tinyint(4) DEFAULT 0 COMMENT '触发业务操作';
ALTER TABLE uk_xiaoe_config ADD busslist text DEFAULT NULL COMMENT '触发业务列表';

ALTER TABLE uk_chat_message ADD bussop tinyint(4) DEFAULT 0 COMMENT '触发业务操作';
ALTER TABLE uk_chat_message ADD busslist text DEFAULT NULL COMMENT '触发业务列表';

ALTER TABLE uk_chat_message ADD bussexec tinyint(4) DEFAULT 0 COMMENT '执行业务操作';
ALTER TABLE uk_chat_message ADD bussexeclist text DEFAULT NULL COMMENT '执行业务列表';


CREATE TABLE `uk_sensitivewords` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `KEYWORD` varchar(50) DEFAULT NULL COMMENT '敏感词',
  `CONTENT` text COMMENT '内容',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `SUPERORDINATE` varchar(50) DEFAULT NULL COMMENT '上位词',
  `PARTOFSPEECH` varchar(50) DEFAULT NULL COMMENT '词性',
  `CATE` varchar(32) DEFAULT NULL COMMENT '分类',
  `type` varchar(32) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='敏感词';

ALTER TABLE uk_sessionconfig ADD responsetimeout TINYINT(4) DEFAULT 0 COMMENT '是否监控响应超时';
ALTER TABLE uk_sessionconfig ADD responsetimeouts INT(11) DEFAULT 0 COMMENT '监控响应超时时长';
ALTER TABLE uk_sessionconfig ADD sestimeout TINYINT(4) DEFAULT 0 COMMENT '是否监控会话超时';
ALTER TABLE uk_sessionconfig ADD sestimeouts INT(11) DEFAULT 0 COMMENT '监控会话超时时长';
ALTER TABLE uk_sessionconfig ADD inquenetimeout TINYINT(4) DEFAULT 0 COMMENT '是否监控排队会话超时';
ALTER TABLE uk_sessionconfig ADD inquenetimeouts INT(11) DEFAULT 0 COMMENT '监控会话排队时长';
ALTER TABLE uk_sessionconfig ADD agentinvalid TINYINT(4) DEFAULT 0 COMMENT '是否监控会话无效';
ALTER TABLE uk_sessionconfig ADD satisfactions TINYINT(4) DEFAULT 0 COMMENT '是否监控会话满意度';
ALTER TABLE uk_sessionconfig ADD satislevels varchar(50) DEFAULT NULL COMMENT '监控满意度级别';



CREATE INDEX index_2 ON uk_chat_message  (`ORGI`,`chatype`,`agentserviceid`,`createtime`,`updatetime`) USING BTREE;
CREATE INDEX index_1 ON uk_onlineuser  (`STATUS`,`orgi`,`createtime`) USING BTREE;

ALTER TABLE uk_agentuser ADD satisfactionalarms INT(11) DEFAULT 0 COMMENT '满意度报警次数';
ALTER TABLE uk_agentuser ADD invitevals INT(11) DEFAULT 0 COMMENT '邀请评价次数';
ALTER TABLE uk_agentuser ADD resptimeouts INT(11) DEFAULT 0 COMMENT '响应超时次数';

DROP TABLE IF EXISTS `wf_cc_order`;
CREATE TABLE `wf_cc_order` (
  `order_Id` varchar(32) DEFAULT NULL COMMENT '流程实例ID',
  `actor_Id` varchar(50) DEFAULT NULL COMMENT '参与者ID',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '抄送时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  KEY `IDX_CCORDER_ORDER` (`order_Id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='抄送实例表';

DROP TABLE IF EXISTS `wf_process`;
CREATE TABLE `wf_process` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `name` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `display_Name` varchar(200) DEFAULT NULL COMMENT '流程显示名称',
  `type` varchar(100) DEFAULT NULL COMMENT '流程类型',
  `instance_Url` varchar(200) DEFAULT NULL COMMENT '实例url',
  `state` tinyint(1) DEFAULT NULL COMMENT '流程是否可用',
  `content` longblob COMMENT '流程模型定义',
  `version` int(2) DEFAULT NULL COMMENT '版本',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_PROCESS_NAME` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='流程定义表';

DROP TABLE IF EXISTS `wf_hist_order`;
CREATE TABLE `wf_hist_order` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `process_Id` varchar(32) NOT NULL COMMENT '流程定义ID',
  `order_State` tinyint(1) NOT NULL COMMENT '状态',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) NOT NULL COMMENT '发起时间',
  `end_Time` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `priority` tinyint(1) DEFAULT NULL COMMENT '优先级',
  `parent_Id` varchar(32) DEFAULT NULL COMMENT '父流程ID',
  `order_No` varchar(50) DEFAULT NULL COMMENT '流程实例编号',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_HIST_ORDER_PROCESSID` (`process_Id`) USING BTREE,
  KEY `IDX_HIST_ORDER_NO` (`order_No`) USING BTREE,
  KEY `FK_HIST_ORDER_PARENTID` (`parent_Id`) USING BTREE,
  CONSTRAINT `wf_hist_order_ibfk_1` FOREIGN KEY (`parent_Id`) REFERENCES `wf_hist_order` (`id`),
  CONSTRAINT `wf_hist_order_ibfk_2` FOREIGN KEY (`process_Id`) REFERENCES `wf_process` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='历史流程实例表';


DROP TABLE IF EXISTS `wf_hist_task`;
CREATE TABLE `wf_hist_task` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(32) NOT NULL COMMENT '流程实例ID',
  `task_Name` varchar(100) NOT NULL COMMENT '任务名称',
  `display_Name` varchar(200) NOT NULL COMMENT '任务显示名称',
  `task_Type` tinyint(1) NOT NULL COMMENT '任务类型',
  `perform_Type` tinyint(1) DEFAULT NULL COMMENT '参与类型',
  `task_State` tinyint(1) NOT NULL COMMENT '任务状态',
  `operator` varchar(50) DEFAULT NULL COMMENT '任务处理人',
  `create_Time` varchar(50) NOT NULL COMMENT '任务创建时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '任务完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '任务期望完成时间',
  `action_Url` varchar(200) DEFAULT NULL COMMENT '任务处理url',
  `parent_Task_Id` varchar(32) DEFAULT NULL COMMENT '父任务ID',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_HIST_TASK_ORDER` (`order_Id`) USING BTREE,
  KEY `IDX_HIST_TASK_TASKNAME` (`task_Name`) USING BTREE,
  KEY `IDX_HIST_TASK_PARENTTASK` (`parent_Task_Id`) USING BTREE,
  CONSTRAINT `wf_hist_task_ibfk_1` FOREIGN KEY (`order_Id`) REFERENCES `wf_hist_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='历史任务表';

DROP TABLE IF EXISTS `wf_hist_task_actor`;
CREATE TABLE `wf_hist_task_actor` (
  `task_Id` varchar(32) NOT NULL COMMENT '任务ID',
  `actor_Id` varchar(50) NOT NULL COMMENT '参与者ID',
  KEY `IDX_HIST_TASKACTOR_TASK` (`task_Id`) USING BTREE,
  CONSTRAINT `wf_hist_task_actor_ibfk_1` FOREIGN KEY (`task_Id`) REFERENCES `wf_hist_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='历史任务参与者表';


DROP TABLE IF EXISTS `wf_order`;
CREATE TABLE `wf_order` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `parent_Id` varchar(32) DEFAULT NULL COMMENT '父流程ID',
  `process_Id` varchar(32) NOT NULL COMMENT '流程定义ID',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) NOT NULL COMMENT '发起时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `last_Update_Time` varchar(50) DEFAULT NULL COMMENT '上次更新时间',
  `last_Updator` varchar(50) DEFAULT NULL COMMENT '上次更新人',
  `priority` tinyint(1) DEFAULT NULL COMMENT '优先级',
  `parent_Node_Name` varchar(100) DEFAULT NULL COMMENT '父流程依赖的节点名称',
  `order_No` varchar(50) DEFAULT NULL COMMENT '流程实例编号',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  `version` int(3) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_ORDER_PROCESSID` (`process_Id`) USING BTREE,
  KEY `IDX_ORDER_NO` (`order_No`) USING BTREE,
  KEY `FK_ORDER_PARENTID` (`parent_Id`) USING BTREE,
  CONSTRAINT `wf_order_ibfk_1` FOREIGN KEY (`parent_Id`) REFERENCES `wf_order` (`id`),
  CONSTRAINT `wf_order_ibfk_2` FOREIGN KEY (`process_Id`) REFERENCES `wf_process` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='流程实例表';



DROP TABLE IF EXISTS `wf_surrogate`;
CREATE TABLE `wf_surrogate` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `process_Name` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `operator` varchar(50) DEFAULT NULL COMMENT '授权人',
  `surrogate` varchar(50) DEFAULT NULL COMMENT '代理人',
  `odate` varchar(64) DEFAULT NULL COMMENT '操作时间',
  `sdate` varchar(64) DEFAULT NULL COMMENT '开始时间',
  `edate` varchar(64) DEFAULT NULL COMMENT '结束时间',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_SURROGATE_OPERATOR` (`operator`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='委托代理表';

DROP TABLE IF EXISTS `wf_task`;
CREATE TABLE `wf_task` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(32) NOT NULL COMMENT '流程实例ID',
  `task_Name` varchar(100) NOT NULL COMMENT '任务名称',
  `display_Name` varchar(200) NOT NULL COMMENT '任务显示名称',
  `task_Type` tinyint(1) NOT NULL COMMENT '任务类型',
  `perform_Type` tinyint(1) DEFAULT NULL COMMENT '参与类型',
  `operator` varchar(50) DEFAULT NULL COMMENT '任务处理人',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '任务创建时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '任务完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '任务期望完成时间',
  `action_Url` varchar(200) DEFAULT NULL COMMENT '任务处理的url',
  `parent_Task_Id` varchar(32) DEFAULT NULL COMMENT '父任务ID',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  `version` tinyint(1) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_TASK_ORDER` (`order_Id`) USING BTREE,
  KEY `IDX_TASK_TASKNAME` (`task_Name`) USING BTREE,
  KEY `IDX_TASK_PARENTTASK` (`parent_Task_Id`) USING BTREE,
  CONSTRAINT `wf_task_ibfk_1` FOREIGN KEY (`order_Id`) REFERENCES `wf_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='任务表';

DROP TABLE IF EXISTS `wf_task_actor`;
CREATE TABLE `wf_task_actor` (
  `task_Id` varchar(32) NOT NULL COMMENT '任务ID',
  `actor_Id` varchar(50) NOT NULL COMMENT '参与者ID',
  KEY `IDX_TASKACTOR_TASK` (`task_Id`) USING BTREE,
  CONSTRAINT `wf_task_actor_ibfk_1` FOREIGN KEY (`task_Id`) REFERENCES `wf_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='任务参与者表';

DROP TABLE IF EXISTS `wf_workitem`;
CREATE TABLE `wf_workitem` (
  `task_id` varchar(255) NOT NULL,
  `process_id` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `process_name` varchar(255) DEFAULT NULL,
  `instance_url` varchar(255) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `order_create_time` varchar(255) DEFAULT NULL,
  `order_expire_time` varchar(255) DEFAULT NULL,
  `order_variable` varchar(255) DEFAULT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `task_key` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `task_create_time` varchar(255) DEFAULT NULL,
  `task_end_time` varchar(255) DEFAULT NULL,
  `task_expire_time` varchar(255) DEFAULT NULL,
  `action_url` varchar(255) DEFAULT NULL,
  `task_type` int(11) DEFAULT NULL,
  `perform_type` int(11) DEFAULT NULL,
  `task_variable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

ALTER TABLE uk_consult_invite ADD displayname varchar(50) DEFAULT NULL COMMENT '显示在访客端的用户名';
ALTER TABLE uk_agentservice ADD transagent tinyint(4) DEFAULT 0 COMMENT '是否转人工（1转人工）';

ALTER TABLE uk_chat_message ADD createdate varchar(50) DEFAULT 'null' COMMENT '创建时间';

ALTER TABLE uk_crm_datamodel ADD tiptext varchar(255) DEFAULT NULL COMMENT '提醒内容';
ALTER TABLE uk_databasetask MODIFY lastupdate datetime DEFAULT null COMMENT '创建时间';

ALTER TABLE uk_contacts ADD cbirthday varchar(50) DEFAULT null COMMENT '生日';

ALTER TABLE uk_callcenter_siptrunk ADD prefixstrreg varchar(255) DEFAULT NULL COMMENT '加拨前缀规则';
INSERT INTO `uk_sysdic` VALUES ('4028801e697af58c01697b6afe3b0299', '标签类型', 'pub', 'codetype', 'ukewo', 'layui-icon', '402888815e097729015e0999f26e0002', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-3-14 16:56:43', NULL, 1, 0, '402888815e097729015e0999f26e0002', 0, 0, NULL, NULL, NULL, NULL, NULL);


ALTER TABLE uk_historyreport ADD ipaddr varchar(200) DEFAULT null COMMENT '执行任务的IP地址';

ALTER TABLE uk_columnproperties ADD sort tinyint(4) DEFAULT 0 COMMENT '是否允许排序';
ALTER TABLE uk_columnproperties ADD fixed tinyint(4) DEFAULT 0 COMMENT '固定列（left、right）';
ALTER TABLE uk_columnproperties ADD type varchar(50) DEFAULT null COMMENT '列类型'; 

ALTER TABLE uk_columnproperties ADD minwidth varchar(50) DEFAULT null COMMENT '最小宽度';


CREATE TABLE `uk_weixinmenu` (
                               `id` varchar(32) NOT NULL COMMENT '主键ID',
                               `snsid` varchar(32) DEFAULT NULL COMMENT 'SNSID',
                               `parentid` varchar(32) DEFAULT NULL COMMENT '父菜单id',
                               `type` varchar(100) DEFAULT NULL COMMENT '菜单类型',
                               `name` varchar(50) DEFAULT NULL COMMENT '一级菜单最多4个汉字，二级菜单最多7个汉字',
                               `keyv` varchar(255) DEFAULT NULL COMMENT 'click等点击类型必须	菜单KEY值，用于消息接口推送，不超过128字节',
                               `url` varchar(255) DEFAULT NULL COMMENT 'view、miniprogram类型必须	网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时',
                               `mediaid` varchar(255) DEFAULT NULL COMMENT 'media_id	media_id类型和view_limited类型必须	调用新增永久素材接口返回的合法me',
                               `appid` varchar(255) DEFAULT NULL COMMENT 'appid	miniprogram类型必须	小程序的appid（仅认证公众号可配置）',
                               `pagepath` varchar(255) DEFAULT NULL COMMENT 'pagepath	miniprogram类型必须	小程序的页面路径',
                               `sortindex` int(10) DEFAULT NULL COMMENT '排序',
                               `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
                               `createtime` datetime DEFAULT NULL COMMENT '创建时间',
                               `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='微信自定义菜单表';

ALTER TABLE uk_columnproperties ADD dbtableid varchar(32) DEFAULT null COMMENT '元数据id'; 

ALTER TABLE uk_sessionconfig ADD transtip tinyint(4) DEFAULT 0 COMMENT '启用转接提示'; 
ALTER TABLE uk_sessionconfig ADD transtiptitle varchar(100) DEFAULT NULL COMMENT '启用转接提示标题'; 
ALTER TABLE uk_sessionconfig ADD transtipmsg varchar(255) DEFAULT NULL COMMENT '启用转接提示默认内容'; 
ALTER TABLE uk_sessionconfig ADD transtiptimeout int(11) DEFAULT 5000 COMMENT '转接提示默认超时时长（毫秒）'; 

ALTER TABLE uk_user ADD bindext tinyint(4) DEFAULT 0 COMMENT '坐席启用分机绑定'; 
ALTER TABLE uk_user ADD hostid varchar(32) DEFAULT NULL COMMENT '坐席绑定的平台ID'; 
ALTER TABLE uk_user ADD extid varchar(32) DEFAULT NULL COMMENT '坐席绑定的分机ID'; 
ALTER TABLE uk_user ADD extno varchar(32) DEFAULT NULL COMMENT '坐席绑定的分机号码';

ALTER TABLE uk_callcenter_extention ADD userid varchar(32) DEFAULT NULL COMMENT '分机绑定的坐席ID';
ALTER TABLE uk_callcenter_extention ADD uname varchar(50) DEFAULT NULL COMMENT '分机绑定的坐席姓名';
ALTER TABLE uk_callcenter_extention ADD username varchar(50) DEFAULT NULL COMMENT '分机绑定的坐席用户名';

ALTER TABLE uk_tableproperties ADD sort tinyint(4) DEFAULT 0 COMMENT '是否允许排序';
ALTER TABLE uk_tableproperties ADD fixed tinyint(4) DEFAULT 0 COMMENT '固定列（left、right）';
ALTER TABLE uk_tableproperties ADD width varchar(50) DEFAULT null COMMENT '宽度';
ALTER TABLE uk_tableproperties ADD minwidth varchar(50) DEFAULT null COMMENT '最小宽度';
ALTER TABLE uk_tableproperties ADD alignment varchar(255) DEFAULT NULL COMMENT '对齐方式';
ALTER TABLE uk_tableproperties ADD unresize tinyint(4) DEFAULT 0 COMMENT '是否禁用拖拽列宽';

ALTER TABLE uk_columnproperties ADD datatypename varchar(255) DEFAULT NULL COMMENT '字段类型名称';
ALTER TABLE uk_columnproperties ADD unresize tinyint(4) DEFAULT 0 COMMENT '是否禁用拖拽列宽';

ALTER TABLE uk_columnproperties ADD seldatacode varchar(255) DEFAULT NULL COMMENT '字典项';
ALTER TABLE uk_columnproperties ADD seldata tinyint(4) DEFAULT 0 COMMENT '是否启用字典';

ALTER TABLE uk_columnproperties ADD required tinyint(4) DEFAULT 0 COMMENT '是否必填';
ALTER TABLE uk_columnproperties ADD readonly tinyint(4) DEFAULT 0 COMMENT '是否只读';
ALTER TABLE uk_columnproperties ADD placeholder varchar(255) DEFAULT NULL COMMENT '占位符';
ALTER TABLE uk_crm_datamodel ADD sorttype varchar(255) DEFAULT NULL COMMENT '排列方式';
ALTER TABLE uk_crm_datamodel ADD sortcol int(11) DEFAULT 1 COMMENT '列数';


ALTER TABLE uk_agentservice ADD transagenttime datetime DEFAULT NULL COMMENT '转人工坐席时间';
ALTER TABLE uk_agentservice ADD transtraceid varchar(32) DEFAULT NULL COMMENT '转人工坐席服务ID';
ALTER TABLE uk_agentservice ADD fromai tinyint(4) DEFAULT 0 COMMENT '是否来自于机器人服务';

ALTER TABLE uk_agentservice ADD aiserviceid varchar(32) DEFAULT NULL COMMENT '机器人服务ID';

ALTER TABLE uk_crm_datamodel ADD actiontypefield varchar(50) DEFAULT NULL COMMENT '布局位置：单元格字段名';

ALTER TABLE uk_systemconfig ADD enableoss TINYINT(4) DEFAULT 0 COMMENT '是否启用对象存储OSS';
ALTER TABLE uk_callcenter_event ADD ossstatus varchar(4) DEFAULT '0' COMMENT '//录音文件是否上传到oss 0否 1是 2文件不存在';


ALTER TABLE uk_systemconfig ADD cloudserverupdatecheckurl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-更新检查-url';
ALTER TABLE uk_systemconfig ADD cloudserveruploadossurl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-上传到oss-url';
ALTER TABLE uk_systemconfig ADD cloudservergetosssizeurl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-获取客户oss容量-url';
ALTER TABLE uk_systemconfig ADD cloudservergetossobjecturl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-获取oss对象-url';

ALTER TABLE uk_agentuser ADD transfer tinyint DEFAULT 0 COMMENT '是否在转接状态';
ALTER TABLE uk_agentuser ADD transfertime datetime DEFAULT NULL COMMENT '转接超时时间';

ALTER TABLE uk_chat_message ADD debug tinyint(4) DEFAULT 0 COMMENT '开发模式';


ALTER TABLE uk_callcenter_event ADD coreuuid varchar(50) DEFAULT NULL COMMENT 'FS标识';

ALTER TABLE uk_xiaoe_sceneitem ADD inputcon text DEFAULT NULL COMMENT '输入条件';
ALTER TABLE uk_xiaoe_sceneitem ADD outputcon text DEFAULT NULL COMMENT '输出条件';

CREATE INDEX index_101 ON uk_chat_message  ('contextid') USING BTREE;
CREATE INDEX index_102 ON uk_chat_message  ('orgi') USING BTREE;
CREATE INDEX index_103 ON uk_chat_message  ('updatetime') USING BTREE;
CREATE INDEX index_200 ON uk_callcenter_event  (`code`) USING BTREE;
CREATE INDEX index_201 ON uk_callcenter_event  (`datestr`) USING BTREE;
CREATE INDEX index_202 ON uk_callcenter_event  (`hourstr`) USING BTREE;
CREATE INDEX index_203 ON uk_callcenter_event  (`contactsid`) USING BTREE;
CREATE INDEX index_204 ON uk_callcenter_event  (`orgi`) USING BTREE;
CREATE INDEX index_205 ON uk_callcenter_event  (`transtatus`) USING BTREE;
CREATE INDEX index_206 ON uk_callcenter_event  (`transbegin`) USING BTREE;
CREATE INDEX index_207 ON uk_callcenter_event  (`servicestatus`) USING BTREE;
CREATE INDEX index_208 ON uk_callcenter_event  (`createtime`) USING BTREE;
CREATE INDEX index_209 ON uk_callcenter_event  (`starttime`) USING BTREE;
CREATE INDEX index_210 ON uk_callcenter_event  (`discalled`) USING BTREE;
CREATE INDEX index_211 ON uk_callcenter_event  (`discaller`) USING BTREE;
CREATE INDEX index_212 ON uk_callcenter_event  (`srecord`) USING BTREE; 
CREATE INDEX index_214 ON uk_callcenter_event  (`satisfaction`) USING BTREE; 
CREATE INDEX index_215 ON uk_callcenter_event  (`satisf`) USING BTREE; 
CREATE INDEX index_216 ON uk_callcenter_event  (`misscall`) USING BTREE; 

CREATE INDEX index_300 ON uk_propertiesevent  (`dataid`) USING BTREE; 
CREATE INDEX index_301 ON uk_propertiesevent  (`createtime`) USING BTREE; 
CREATE INDEX index_302 ON uk_propertiesevent  (`field`) USING BTREE; 


CREATE INDEX index_400 ON uk_propertiesevent  (`userid`) USING BTREE; 
CREATE INDEX index_401 ON uk_propertiesevent  (`aiservice`) USING BTREE; 
CREATE INDEX index_402 ON uk_propertiesevent  (`orgi`) USING BTREE; 
CREATE INDEX index_403 ON uk_propertiesevent  (`endtime`) USING BTREE; 

ALTER TABLE `uk_system_updatecon`
	MODIFY COLUMN `jarurl` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'war包下载' ,
	MODIFY COLUMN `sqlurl` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '脚本下载url';



ALTER TABLE uk_consult_invite ADD skillgroup text DEFAULT NULL COMMENT '技能组';


ALTER TABLE uk_chat_message ADD mround tinyint(4) DEFAULT 0 COMMENT '启用多轮对话';
ALTER TABLE uk_chat_message ADD quesid varchar(50) DEFAULT NULL COMMENT '多轮对话节点ID';
ALTER TABLE uk_chat_message ADD dataid varchar(50) DEFAULT NULL COMMENT '多轮对话ID';
ALTER TABLE uk_chat_message ADD mtype varchar(50) DEFAULT NULL COMMENT '多轮对话来源类型';
ALTER TABLE uk_chat_message ADD createtime datetime DEFAULT NULL COMMENT '创建时间';


ALTER TABLE uk_callcenter_event ADD inqueuetime datetime DEFAULT NULL COMMENT '进入队列时间';
ALTER TABLE uk_callcenter_event ADD outqueuetime datetime DEFAULT NULL COMMENT '出队列时间';

ALTER TABLE uk_callcenter_event ADD queuetime int DEFAULT 0 COMMENT '排队时长';



ALTER TABLE uk_xiaoe_config ADD bussexec tinyint(4) DEFAULT 0 COMMENT '执行业务操作';
ALTER TABLE uk_xiaoe_config ADD bussexeclist text DEFAULT NULL COMMENT '执行业务列表';

ALTER TABLE uk_xiaoe_config ADD bussskill varchar(50) DEFAULT NULL COMMENT '转人工分配技能组';
ALTER TABLE uk_xiaoe_config ADD bussagent varchar(50) DEFAULT NULL COMMENT '转人工分配坐席';
ALTER TABLE uk_xiaoe_config ADD busswo varchar(50) DEFAULT NULL COMMENT '创建工单的工单模板';
ALTER TABLE uk_xiaoe_config ADD busswoorgan varchar(50) DEFAULT NULL COMMENT '创建工单的分配部门';
ALTER TABLE uk_xiaoe_config ADD busswouser varchar(50) DEFAULT NULL COMMENT '创建工单的分配坐席';

ALTER TABLE uk_xiaoe_config ADD bussmsg tinyint(4) DEFAULT 0 COMMENT '创建工单时发送短信';
ALTER TABLE uk_xiaoe_config ADD bussmsgtpl varchar(50) DEFAULT NULL COMMENT '创建工单时发送短信的模板';
ALTER TABLE uk_xiaoe_config ADD bussname varchar(100) DEFAULT NULL COMMENT '信息采集补全时默认姓名';
ALTER TABLE uk_xiaoe_config ADD bussmapping text DEFAULT NULL COMMENT '业务分类映射部门';
ALTER TABLE uk_xiaoe_config ADD bussdisall tinyint(4) DEFAULT 0 COMMENT '共享联系人';

ALTER TABLE uk_xiaoe_config ADD sessiontimeout tinyint(4) DEFAULT 0 COMMENT '启用会话超时提醒';
ALTER TABLE uk_xiaoe_config ADD timeout int(11) DEFAULT 0 COMMENT '会话超时提醒时间';
ALTER TABLE uk_xiaoe_config ADD timeoutmsg text DEFAULT NULL COMMENT '会话超时提醒消息';

ALTER TABLE uk_xiaoe_config ADD resessiontimeout tinyint(4) DEFAULT 0 COMMENT '启用会话二次超时提醒';
ALTER TABLE uk_xiaoe_config ADD retimeout int(11) DEFAULT 0 COMMENT '会话再次超时提醒时间';
ALTER TABLE uk_xiaoe_config ADD retimeoutmsg text DEFAULT NULL COMMENT '会话再次超时提醒消息';


ALTER TABLE uk_contacts ADD sourcetype varchar(50) DEFAULT NULL COMMENT '数据来源类型';
ALTER TABLE uk_contacts ADD dataid varchar(50) DEFAULT NULL COMMENT '数据来源ID';


ALTER TABLE uk_user ADD lastdisdate datetime DEFAULT NULL COMMENT '最近商机分配时间';
ALTER TABLE uk_user ADD disnum int DEFAULT 0 COMMENT '累计商机分配数量';

ALTER TABLE uk_user ADD lastworkorderdate datetime DEFAULT NULL COMMENT '最近工单分配时间';
ALTER TABLE uk_user ADD workordernum int DEFAULT 0 COMMENT '累计工单分配数量';


ALTER TABLE uk_user ADD workorder tinyint(4) DEFAULT 0 COMMENT '启用工单分配';
ALTER TABLE uk_user ADD bussopdis tinyint(4) DEFAULT 0 COMMENT '启用商机分配';


CREATE TABLE complete (
   sticky  INTEGER,
   a1  VARCHAR(128),
   a2  VARCHAR(128),
   a3  VARCHAR(128),
   a4  VARCHAR(128),
   a5  VARCHAR(128),
   a6  VARCHAR(128),
   a7  VARCHAR(128),
   a8  VARCHAR(128),
   a9  VARCHAR(128),
   a10 VARCHAR(128),
   hostname VARCHAR(256)
);
CREATE TABLE aliases (
   sticky  INTEGER,
   alias  VARCHAR(128),
   command  VARCHAR(4096),
   hostname VARCHAR(256)
);


CREATE TABLE channels (
   uuid  VARCHAR(256),
   direction  VARCHAR(32),
   created  VARCHAR(128),
   created_epoch  INTEGER,
   name  text,
   state  VARCHAR(64),
   cid_name  VARCHAR(1024),
   cid_num  VARCHAR(256),
   ip_addr  VARCHAR(256),
   dest  text,
   application  VARCHAR(128),
   application_data  text,
   dialplan VARCHAR(128),
   context VARCHAR(128),
   read_codec  VARCHAR(128),
   read_rate  VARCHAR(32),
   read_bit_rate  VARCHAR(32),
   write_codec  VARCHAR(128),
   write_rate  VARCHAR(32),
   write_bit_rate  VARCHAR(32),
   secure VARCHAR(64),
   hostname VARCHAR(256),
   presence_id text,
   presence_data text,
   accountcode VARCHAR(256),
   callstate  VARCHAR(64),
   callee_name  text,
   callee_num  VARCHAR(256),
   callee_direction  VARCHAR(5),
   call_uuid  VARCHAR(256),
   sent_callee_name  text,
   sent_callee_num  VARCHAR(256),
   initial_cid_name  text,
   initial_cid_num  VARCHAR(256),
   initial_ip_addr  VARCHAR(256),
   initial_dest  text,
   initial_dialplan  VARCHAR(128),
   initial_context  VARCHAR(128)
);

CREATE TABLE calls (
   call_uuid  VARCHAR(255),
   call_created  VARCHAR(128),
   call_created_epoch  INTEGER,
   caller_uuid      VARCHAR(256),
   callee_uuid      VARCHAR(256),
   hostname VARCHAR(256)
);

CREATE TABLE interfaces (
   type             VARCHAR(128),
   name             VARCHAR(1024),
   description      VARCHAR(4096),
   ikey             VARCHAR(1024),
   filename         VARCHAR(4096),
   syntax           VARCHAR(4096),
   hostname VARCHAR(256)
);

CREATE TABLE tasks (
   task_id             int,
   task_desc           VARCHAR(4096),
   task_group          VARCHAR(1024),
   task_runtime        int,
   task_sql_manager    int,
   hostname            VARCHAR(256)
);

CREATE TABLE nat (
   sticky  int,
	port	int,
	proto	int,
   hostname VARCHAR(256)
);


CREATE TABLE registrations (
   reg_user      VARCHAR(256),
   realm     VARCHAR(256),
   token     VARCHAR(256),
	/* If url is modified please check for code in switch_core_sqldb_start for dependencies for MSSQL" */
   url      TEXT,
   expires  INTEGER,
   network_ip VARCHAR(256),
   network_port VARCHAR(256),
   network_proto VARCHAR(256),
   hostname VARCHAR(256),
   metadata VARCHAR(256)
);
	
CREATE TABLE recovery (
   runtime_uuid    VARCHAR(255),
   technology      VARCHAR(255),
   profile_name    VARCHAR(255),
   hostname        VARCHAR(255),
   uuid            VARCHAR(255),
   metadata        text
);


create view detailed_calls as select 
a.uuid as uuid,
a.direction as direction,
a.created as created,
a.created_epoch as created_epoch,
a.name as name,
a.state as state,
a.cid_name as cid_name,
a.cid_num as cid_num,
a.ip_addr as ip_addr,
a.dest as dest,
a.application as application,
a.application_data as application_data,
a.dialplan as dialplan,
a.context as context,
a.read_codec as read_codec,
a.read_rate as read_rate,
a.read_bit_rate as read_bit_rate,
a.write_codec as write_codec,
a.write_rate as write_rate,
a.write_bit_rate as write_bit_rate,
a.secure as secure,
a.hostname as hostname,
a.presence_id as presence_id,
a.presence_data as presence_data,
a.accountcode as accountcode,
a.callstate as callstate,
a.callee_name as callee_name,
a.callee_num as callee_num,
a.callee_direction as callee_direction,
a.call_uuid as call_uuid,
a.sent_callee_name as sent_callee_name,
a.sent_callee_num as sent_callee_num,
b.uuid as b_uuid,
b.direction as b_direction,
b.created as b_created,
b.created_epoch as b_created_epoch,
b.name as b_name,
b.state as b_state,
b.cid_name as b_cid_name,
b.cid_num as b_cid_num,
b.ip_addr as b_ip_addr,
b.dest as b_dest,
b.application as b_application,
b.application_data as b_application_data,
b.dialplan as b_dialplan,
b.context as b_context,
b.read_codec as b_read_codec,
b.read_rate as b_read_rate,
b.read_bit_rate as b_read_bit_rate,
b.write_codec as b_write_codec,
b.write_rate as b_write_rate,
b.write_bit_rate as b_write_bit_rate,
b.secure as b_secure,
b.hostname as b_hostname,
b.presence_id as b_presence_id,
b.presence_data as b_presence_data,
b.accountcode as b_accountcode,
b.callstate as b_callstate,
b.callee_name as b_callee_name,
b.callee_num as b_callee_num,
b.callee_direction as b_callee_direction,
b.call_uuid as b_call_uuid,
b.sent_callee_name as b_sent_callee_name,
b.sent_callee_num as b_sent_callee_num,
c.call_created_epoch as call_created_epoch 
from channels a 
left join calls c on a.uuid = c.caller_uuid and a.hostname = c.hostname 
left join channels b on b.uuid = c.callee_uuid and b.hostname = c.hostname 
where a.uuid = c.caller_uuid or a.uuid not in (select callee_uuid from calls);



create view basic_calls as select 
a.uuid as uuid,
a.direction as direction,
a.created as created,
a.created_epoch as created_epoch,
a.name as name,
a.state as state,
a.cid_name as cid_name,
a.cid_num as cid_num,
a.ip_addr as ip_addr,
a.dest as dest,

a.presence_id as presence_id,
a.presence_data as presence_data,
a.accountcode as accountcode,
a.callstate as callstate,
a.callee_name as callee_name,
a.callee_num as callee_num,
a.callee_direction as callee_direction,
a.call_uuid as call_uuid,
a.hostname as hostname,
a.sent_callee_name as sent_callee_name,
a.sent_callee_num as sent_callee_num,


b.uuid as b_uuid,
b.direction as b_direction,
b.created as b_created,
b.created_epoch as b_created_epoch,
b.name as b_name,
b.state as b_state,
b.cid_name as b_cid_name,
b.cid_num as b_cid_num,
b.ip_addr as b_ip_addr,
b.dest as b_dest,
	
b.presence_id as b_presence_id,
b.presence_data as b_presence_data,
b.accountcode as b_accountcode,
b.callstate as b_callstate,
b.callee_name as b_callee_name,
b.callee_num as b_callee_num,
b.callee_direction as b_callee_direction,
b.sent_callee_name as b_sent_callee_name,
b.sent_callee_num as b_sent_callee_num,
c.call_created_epoch as call_created_epoch 

from channels a 
left join calls c on a.uuid = c.caller_uuid and a.hostname = c.hostname 
left join channels b on b.uuid = c.callee_uuid and b.hostname = c.hostname 
where a.uuid = c.caller_uuid or a.uuid not in (select callee_uuid from calls);




CREATE TABLE sip_registrations (
   call_id          VARCHAR(255),
   sip_user         VARCHAR(255),
   sip_host         VARCHAR(255),
   presence_hosts   VARCHAR(255),
   contact          VARCHAR(1024),
   status           VARCHAR(255),
   ping_status      VARCHAR(255),
   ping_count       INTEGER,
   ping_time        BIGINT,
   force_ping       INTEGER,
   rpid             VARCHAR(255),
   expires          BIGINT,
   ping_expires     INTEGER not null default 0,
   user_agent       VARCHAR(255),
   server_user      VARCHAR(255),
   server_host      VARCHAR(255),
   profile_name     VARCHAR(255),
   hostname         VARCHAR(255),
   network_ip       VARCHAR(255),
   network_port     VARCHAR(6),
   sip_username     VARCHAR(255),
   sip_realm        VARCHAR(255),
   mwi_user         VARCHAR(255),
   mwi_host         VARCHAR(255),
   orig_server_host VARCHAR(255),
   orig_hostname    VARCHAR(255),
   sub_host         VARCHAR(255)
);


CREATE TABLE sip_presence (
   sip_user        VARCHAR(255),
   sip_host        VARCHAR(255),
   status          VARCHAR(255),
   rpid            VARCHAR(255),
   expires         BIGINT,
   user_agent      VARCHAR(255),
   profile_name    VARCHAR(255),
   hostname        VARCHAR(255),
   network_ip      VARCHAR(255),
   network_port    VARCHAR(6),
   open_closed     VARCHAR(255)
);


CREATE TABLE sip_dialogs (
   call_id         VARCHAR(255),
   uuid            VARCHAR(255),
   sip_to_user     VARCHAR(255),
   sip_to_host     VARCHAR(255),
   sip_from_user   VARCHAR(255),
   sip_from_host   VARCHAR(255),
   contact_user    VARCHAR(255),
   contact_host    VARCHAR(255),
   state           VARCHAR(255),
   direction       VARCHAR(255),
   user_agent      VARCHAR(255),
   profile_name    VARCHAR(255),
   hostname        VARCHAR(255),
   contact         VARCHAR(255),
   presence_id     VARCHAR(255),
   presence_data   VARCHAR(255),
   call_info       VARCHAR(255),
   call_info_state VARCHAR(255) default '',
   expires         BIGINT default 0,
   status          VARCHAR(255),
   rpid            VARCHAR(255),
   sip_to_tag      VARCHAR(255),
   sip_from_tag    VARCHAR(255),
   rcd             INTEGER not null default 0
);


CREATE TABLE sip_subscriptions (
   proto           VARCHAR(255),
   sip_user        VARCHAR(255),
   sip_host        VARCHAR(255),
   sub_to_user     VARCHAR(255),
   sub_to_host     VARCHAR(255),
   presence_hosts  VARCHAR(255),
   event           VARCHAR(255),
   contact         VARCHAR(1024),
   call_id         VARCHAR(255),
   full_from       VARCHAR(255),
   full_via        VARCHAR(255),
   expires         BIGINT,
   user_agent      VARCHAR(255),
   accept          VARCHAR(255),
   profile_name    VARCHAR(255),
   hostname        VARCHAR(255),
   network_port    VARCHAR(6),
   network_ip      VARCHAR(255),
   version         INTEGER DEFAULT 0 NOT NULL,
   orig_proto      VARCHAR(255),
   full_to         VARCHAR(255)
);


CREATE TABLE sip_authentication (
   nonce           VARCHAR(255),
   expires         BIGINT,
   profile_name    VARCHAR(255),
   hostname        VARCHAR(255),
   last_nc         INTEGER
);


CREATE TABLE sip_shared_appearance_subscriptions (
   subscriber        VARCHAR(255),
   call_id           VARCHAR(255),
   aor               VARCHAR(255),
   profile_name      VARCHAR(255),
   hostname          VARCHAR(255),
   contact_str       VARCHAR(255),
   network_ip        VARCHAR(255)
);


CREATE TABLE sip_shared_appearance_dialogs (
   profile_name      VARCHAR(255),
   hostname          VARCHAR(255),
   contact_str       VARCHAR(255),
   call_id           VARCHAR(255),
   network_ip        VARCHAR(255),
   expires           BIGINT
);

create index ss_call_id on sip_subscriptions (call_id);
create index ss_multi on sip_subscriptions (call_id, profile_name, hostname);
create index ss_hostname on sip_subscriptions (hostname);
create index ss_network_ip on sip_subscriptions (network_ip);
create index ss_sip_user on sip_subscriptions (sip_user);
create index ss_sip_host on sip_subscriptions (sip_host);
create index ss_presence_hosts on sip_subscriptions (presence_hosts);
create index ss_event on sip_subscriptions (event);
create index ss_proto on sip_subscriptions (proto);
create index ss_sub_to_user on sip_subscriptions (sub_to_user);
create index ss_sub_to_host on sip_subscriptions (sub_to_host);
create index ss_expires on sip_subscriptions (expires);
create index ss_orig_proto on sip_subscriptions (orig_proto);
create index ss_network_port on sip_subscriptions (network_port);
create index ss_profile_name on sip_subscriptions (profile_name);
create index ss_version on sip_subscriptions (version);
create index ss_full_from on sip_subscriptions (full_from);
create index ss_contact on sip_subscriptions (contact);
create index sd_uuid on sip_dialogs (uuid);
create index sd_hostname on sip_dialogs (hostname);
create index sd_presence_data on sip_dialogs (presence_data);
create index sd_call_info on sip_dialogs (call_info);
create index sd_call_info_state on sip_dialogs (call_info_state);
create index sd_expires on sip_dialogs (expires);
create index sd_rcd on sip_dialogs (rcd);
create index sd_sip_to_tag on sip_dialogs (sip_to_tag);
create index sd_sip_from_user on sip_dialogs (sip_from_user);
create index sd_sip_from_host on sip_dialogs (sip_from_host);
create index sd_sip_to_host on sip_dialogs (sip_to_host);
create index sd_presence_id on sip_dialogs (presence_id);
create index sd_call_id on sip_dialogs (call_id);
create index sd_sip_from_tag on sip_dialogs (sip_from_tag);
create index sp_hostname on sip_presence (hostname);
create index sp_open_closed on sip_presence (open_closed);
create index sp_sip_user on sip_presence (sip_user);
create index sp_sip_host on sip_presence (sip_host);
create index sp_profile_name on sip_presence (profile_name);
create index sp_expires on sip_presence (expires);
create index sa_nonce on sip_authentication (nonce);
create index sa_hostname on sip_authentication (hostname);
create index sa_expires on sip_authentication (expires);
create index sa_last_nc on sip_authentication (last_nc);
create index ssa_hostname on sip_shared_appearance_subscriptions (hostname);
create index ssa_network_ip on sip_shared_appearance_subscriptions (network_ip);
create index ssa_subscriber on sip_shared_appearance_subscriptions (subscriber);
create index ssa_profile_name on sip_shared_appearance_subscriptions (profile_name);
create index ssa_aor on sip_shared_appearance_subscriptions (aor);
create index ssd_profile_name on sip_shared_appearance_dialogs (profile_name);
create index ssd_hostname on sip_shared_appearance_dialogs (hostname);
create index ssd_contact_str on sip_shared_appearance_dialogs (contact_str);
create index ssd_call_id on sip_shared_appearance_dialogs (call_id);
create index ssd_expires on sip_shared_appearance_dialogs (expires);
create index uuid1 on channels (uuid);


create table fifo_outbound (
 uuid varchar(255),
 fifo_name varchar(255),
 originate_string varchar(255),
 simo_count integer,
 use_count integer,
 timeout integer,
 lag integer,
 next_avail integer not null default 0,
 expires integer not null default 0,
 static integer not null default 0,
 outbound_call_count integer not null default 0,
 outbound_fail_count integer not null default 0,
 hostname varchar(255),
 taking_calls integer not null default 1,
 status varchar(255),
 outbound_call_total_count integer not null default 0,
 outbound_fail_total_count integer not null default 0,
 active_time integer not null default 0,
 inactive_time integer not null default 0,
 manual_calls_out_count integer not null default 0,
 manual_calls_in_count integer not null default 0,
 manual_calls_out_total_count integer not null default 0,
 manual_calls_in_total_count integer not null default 0,
 ring_count integer not null default 0,
 start_time integer not null default 0,
 stop_time integer not null default 0
);
create table fifo_bridge (
 fifo_name varchar(1024) not null,
 caller_uuid varchar(255) not null,
 caller_caller_id_name varchar(255),
 caller_caller_id_number varchar(255),

 consumer_uuid varchar(255) not null,
 consumer_outgoing_uuid varchar(255),
 bridge_start integer
);
create table fifo_callers (
 fifo_name varchar(255) not null,
 uuid varchar(255) not null,
 caller_caller_id_name varchar(255),
 caller_caller_id_number varchar(255),
 timestamp integer
);


CREATE TABLE voicemail_msgs (
   created_epoch INTEGER,
   read_epoch    INTEGER,
   username      VARCHAR(255),
   domain        VARCHAR(255),
   uuid          VARCHAR(255),
   cid_name      VARCHAR(255),
   cid_number    VARCHAR(255),
   in_folder     VARCHAR(255),
   file_path     VARCHAR(255),
   message_len   INTEGER, 
   flags         VARCHAR(255), 
   read_flags    VARCHAR(255), 
   forwarded_by  VARCHAR(255) 
);
CREATE TABLE voicemail_prefs (
   username        VARCHAR(255),
   domain          VARCHAR(255),
   name_path       VARCHAR(255), 
   greeting_path   VARCHAR(255), 
   password        VARCHAR(255)
);

create index voicemail_msgs_idx1 on voicemail_msgs(created_epoch);
create index voicemail_msgs_idx2 on voicemail_msgs(username);
create index voicemail_msgs_idx3 on voicemail_msgs(domain);
create index voicemail_msgs_idx4 on voicemail_msgs(uuid);
create index voicemail_msgs_idx5 on voicemail_msgs(in_folder);
create index voicemail_msgs_idx6 on voicemail_msgs(read_flags);
create index voicemail_msgs_idx7 on voicemail_msgs(forwarded_by);
create index voicemail_msgs_idx8 on voicemail_msgs(read_epoch);
create index voicemail_msgs_idx9 on voicemail_msgs(flags);
create index voicemail_prefs_idx1 on voicemail_prefs(username);
create index voicemail_prefs_idx2 on voicemail_prefs(domain);



create table json_store (
 name varchar(255) not null,
 data text
);


CREATE TABLE limit_data (
   hostname   VARCHAR(255),
   realm      VARCHAR(255),
   id         VARCHAR(255),
   uuid       VARCHAR(255)
);

CREATE TABLE db_data (
   hostname   VARCHAR(255),
   realm      VARCHAR(255),
   data_key   VARCHAR(255),
   data       VARCHAR(255)
);

CREATE TABLE group_data (
   hostname   VARCHAR(255),
   groupname  VARCHAR(255),
   url        VARCHAR(255)
);


-- ----------------------------
-- 2019 04 23 添加
-- ----------------------------
ALTER TABLE uk_systemconfig ADD agentnum INT(11) DEFAULT 0 COMMENT '文本坐席数量 （云平台坐席限制使用）';
ALTER TABLE uk_systemconfig ADD callcenteragentnum INT(11) DEFAULT 0 COMMENT '呼叫中心坐席数量 （云平台坐席限制使用）';

-- ----------------------------
-- 2019 04 25 添加
-- ----------------------------
ALTER TABLE uk_workorders MODIFY ORDERNO VARCHAR(50) DEFAULT null COMMENT '工单编号';

ALTER TABLE uk_callcenter_pbxhost ADD gwlua VARCHAR(200) DEFAULT 'ukefu-sub-gateway.lua' COMMENT '呼修改网关 的lua脚本名称';

ALTER TABLE uk_xiaoe_config ADD userdefinescore int DEFAULT 20 COMMENT '自定义关键词权重';

ALTER TABLE uk_xiaoe_config ADD keywordscore int DEFAULT 5 COMMENT '识别关键词权重';

ALTER TABLE uk_chat_message ADD matchnum int DEFAULT 0 COMMENT '匹配数量';
ALTER TABLE uk_chat_message ADD correct tinyint DEFAULT 0 COMMENT '匹配正确';
ALTER TABLE uk_chat_message ADD tagget tinyint DEFAULT 0 COMMENT '已标记';

-- ----------------------------
-- 2019 05 05 添加
-- ----------------------------
ALTER TABLE uk_xiaoe_config ADD artificial text COMMENT '转人工方式';
ALTER TABLE uk_xiaoe_config ADD phonemsg text COMMENT '电话咨询提示语';
ALTER TABLE uk_xiaoe_config ADD greeting text COMMENT '顶部欢迎语';
ALTER TABLE uk_xiaoe_config ADD warmmsg text COMMENT '温馨提示语';
ALTER TABLE uk_xiaoe_config ADD speartificial text COMMENT '特殊渠道，转人工方式';

CREATE TABLE `uk_ai_appointmentcall` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `userid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `aiid` varchar(32) DEFAULT NULL COMMENT '机器人id',
  `name` text COMMENT '预约姓名',
  `phone` text COMMENT '预约电话',
  `question` text COMMENT '问题描述',
  `source` varchar(50) DEFAULT NULL COMMENT '渠道来源',
  `product` varchar(50) DEFAULT NULL COMMENT '咨询产品',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='预约回电表';



ALTER TABLE uk_xiaoe_config ADD topicscore int DEFAULT 10 COMMENT '主题词权重';

ALTER TABLE uk_xiaoe_config ADD titlescore int DEFAULT 1 COMMENT '标题权重';
ALTER TABLE uk_xiaoe_config ADD silimarscore int DEFAULT 10 COMMENT '类似问题权重';


ALTER TABLE uk_xiaoe_config ADD entity tinyint DEFAULT 0 COMMENT '启用实体识别';
ALTER TABLE uk_xiaoe_config ADD entitytp varchar(255) DEFAULT NULL COMMENT '启用的实体类型';
ALTER TABLE uk_xiaoe_config ADD entityscore int DEFAULT 10 COMMENT '实体识别权重';
-- ----------------------------
-- 2019 05 09 添加
-- ----------------------------
CREATE TABLE `uk_number_pool` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `hostid` varchar(32) DEFAULT NULL COMMENT 'hostid',
  `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
  `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
  `datastatus` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  `number` varchar(200) DEFAULT NULL COMMENT '号码',
  `orgi` varchar(200) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='号码池表';

CREATE TABLE `uk_number_pool_extention_rela` (
     `id` varchar(32) NOT NULL COMMENT '主键ID',
     `hostid` varchar(32) DEFAULT NULL COMMENT 'hostid',
     `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
     `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
     `createtime` datetime DEFAULT NULL COMMENT '创建时间',
     `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
     `datastatus` tinyint(4) DEFAULT '0' COMMENT '是否删除',
     `extentionid` varchar(32) DEFAULT NULL COMMENT '分机id',
     `numberpoolid` varchar(32) DEFAULT NULL COMMENT '号码id',
     `orgi` varchar(200) DEFAULT NULL COMMENT '租户id',
     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='号码 分机 关联表';

ALTER TABLE uk_chat_message ADD topictitle varchar(255) DEFAULT NULL COMMENT '命中的 知识库 标题';

ALTER TABLE uk_systemconfig ADD transports varchar(20) DEFAULT NULL COMMENT '默认的通讯协议';
ALTER TABLE uk_consult_invite ADD transports varchar(20) DEFAULT NULL COMMENT '默认的通讯协议';
ALTER TABLE uk_consult_invite ADD writting tinyint DEFAULT 0 COMMENT '禁用用户输入意图功能';


ALTER TABLE uk_consult_invite ADD tipmsgbgcolor varchar(50) DEFAULT NULL COMMENT '访客端提示消息背景颜色';

ALTER TABLE uk_consult_invite ADD aitabtitle varchar(50) DEFAULT NULL COMMENT 'ai页签名称';
ALTER TABLE uk_consult_invite ADD agenttabtitle varchar(50) DEFAULT NULL COMMENT '人工坐席页签名称';
ALTER TABLE uk_consult_invite ADD aititle varchar(255) DEFAULT NULL COMMENT 'AI页面标题';
ALTER TABLE uk_consult_invite ADD agenttitle varchar(255) DEFAULT NULL COMMENT '坐席页面标题';

ALTER TABLE uk_systemconfig ADD enablereqlogwarning tinyint DEFAULT 0 COMMENT '启用审计日志预警功能';
ALTER TABLE uk_systemconfig ADD reqlogwarningemailid varchar(32) DEFAULT NULL COMMENT '审计日志预警电子邮件服务器';
ALTER TABLE uk_systemconfig ADD reqlogwarningemailtp varchar(32) DEFAULT NULL COMMENT '审计日志预警电子邮件默认模板';
ALTER TABLE uk_systemconfig ADD reqlogwarningsmsid varchar(32) DEFAULT NULL COMMENT '审计日志预警短信网关ID';
ALTER TABLE uk_systemconfig ADD reqlogwarningsmstp varchar(32) DEFAULT NULL COMMENT '审计日志预警短信通知的模板';
ALTER TABLE uk_systemconfig ADD reqlogwarningaction text DEFAULT NULL COMMENT '审计日志预警动作';
ALTER TABLE uk_systemconfig ADD reqlogwarningtouser varchar(255) DEFAULT NULL COMMENT '审计日志预警接收人';
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abe95c2016abeaca3a100f4', '拉入黑名单', 'pub', 'blacklist', 'ukewo', 'layui-icon', '402813816abe95c2016abeac22ff00ed', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 11:25:46', NULL, 1, 0, '402813816abe95c2016abeac22ff00ed', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abec2f0016abed5ccc000c3', '禁止该ip访问', 'pub', 'banip', 'ukewo', 'layui-icon', '402813816abe95c2016abeac22ff00ed', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 12:10:43', NULL, 1, 0, '402813816abe95c2016abeac22ff00ed', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfc9aa6003de', '轻微', 'pub', 'mild', 'ukewo', 'layui-icon', '402813816abf6c63016abfc5221303ac', '', NULL, '', '#01AAED', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:37:05', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfc9fa5103e2', '一般', 'pub', 'general', 'ukewo', 'layui-icon', '402813816abf6c63016abfc5221303ac', '', NULL, '', '#FFB800', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:37:26', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfca6db903e8', '严重', 'pub', 'severity', 'ukewo', 'layui-icon', '402813816abf6c63016abfc5221303ac', '', NULL, '', '#FF5722', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:37:55', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfcb462103f3', '参数为空', 'pub', 'paramnull', 'ukewo', 'layui-icon', '402813816abf6c63016abfc9aa6003de', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:38:51', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfcdd13c040f', 'XSS(跨站脚本攻击)', 'pub', 'xss', 'ukewo', 'layui-icon', '402813816abf6c63016abfc9fa5103e2', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:41:37', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfcf3c9c041f', 'Dos攻击', 'pub', 'dos', 'ukewo', 'layui-icon', '402813816abf6c63016abfca6db903e8', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:43:10', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfd0d2750432', '非法字符', 'pub', 'illegalcharacter', 'ukewo', 'layui-icon', '402813816abf6c63016abfc9aa6003de', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:44:54', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);




ALTER TABLE uk_callcenter_event ADD membersessionid varchar(50) DEFAULT NULL COMMENT '转接前通话ID';
-- ----------------------------
-- 2019 05 20 添加
-- ----------------------------
CREATE TABLE `uk_spt_media` (
                                `id` varchar(32) NOT NULL COMMENT '主键ID',
                                `creater` varchar(32) DEFAULT NULL COMMENT '创建人ID',
                                `createtime` datetime DEFAULT NULL COMMENT '创建时间',
                                `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
                                `name` varchar(100) DEFAULT NULL COMMENT '名称',
                                `orgi` varchar(100) DEFAULT NULL COMMENT '租户ID',
                                `salespatterid` varchar(32) DEFAULT NULL COMMENT '话术ID',
                                `type` varchar(32) DEFAULT NULL COMMENT '类型',
                                `filename` varchar(255) DEFAULT NULL COMMENT '文件名',
                                `content` varchar(50) DEFAULT NULL COMMENT '文件类型',
                                `filelength` int(11) DEFAULT NULL COMMENT '语音文件长度',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='话术媒体资源表';



ALTER TABLE uk_callcenter_siptrunk ADD localreg tinyint(4) DEFAULT 0 COMMENT '是否本地注册';
ALTER TABLE uk_log_request ADD triggerwarnings varchar(500) DEFAULT NULL COMMENT '触发警告';
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816add834f016add96df8a00e1', '日志预警', 'pub', 'logwarn', 'ukewo', 'layui-icon', '402813816abf6c63016abfc5221303ac', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-22 11:30:13', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816ae3c690016ae3ca869c0038', '日志报错', 'pub', 'logerror', 'ukewo', 'layui-icon', '402813816add834f016add96df8a00e1', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-23 16:24:21', NULL, 1, 0, '402813816abf6c63016abfc5221303ac', 0, 0, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- 2019 05 22 添加
-- ----------------------------
ALTER TABLE uk_que_survey_process ADD language varchar(50) DEFAULT NULL COMMENT '语种、方言';
ALTER TABLE uk_spt_salespatter ADD language varchar(50) DEFAULT NULL COMMENT '语种、方言';

INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('297eebf16add6d71016ade448e690f17', '英文', 'pub', 'english', 'ukewo', 'layui-icon', '297eebf16add6d71016ade435e1d0f04', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-05-22 14:39:56', '2019-05-22 14:39:56', '0', '2', '297eebf16add6d71016ade435e1d0f04', '0', '1', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('297eebf16add6d71016ade448e700f18', '粤语', 'pub', 'cantonese', 'ukewo', 'layui-icon', '297eebf16add6d71016ade435e1d0f04', '/etc/smartivr_yy.json', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-05-22 14:39:56', '2019-05-22 14:39:56', '0', '3', '297eebf16add6d71016ade435e1d0f04', '0', '1', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('297eebf16add6d71016ade448e600f16', '中文普通话', 'pub', 'mandarin', 'ukewo', 'layui-icon', '297eebf16add6d71016ade435e1d0f04', '/etc/smartivr.json', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-05-22 14:39:55', '2019-05-22 14:39:55', '0', '1', '297eebf16add6d71016ade435e1d0f04', '0', '1', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('297eebf16add6d71016ade435e1d0f04', '语种/方言', 'pub', 'com.dic.sales.lang', 'ukewo', 'data', '0', '话术语种/方言配置', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-05-22 14:38:38', NULL, '1', '0', NULL, '0', '0', NULL, NULL, NULL, NULL, NULL);

ALTER TABLE uk_callcenter_extention ADD language varchar(50) DEFAULT NULL COMMENT '智能机器人 IVR 语种、方言';


ALTER TABLE uk_que_survey_question ADD overtime int(11) DEFAULT 3 COMMENT '超时等待时长';

ALTER TABLE uk_que_survey_answer ADD sortindex int(11) DEFAULT 0 COMMENT '答案排序';

ALTER TABLE uk_que_survey_answer ADD interrupt tinyint(4) DEFAULT 0 COMMENT '是否支持打断';


ALTER TABLE uk_systemconfig ADD cleanxss tinyint(4) DEFAULT 0 COMMENT '启用XSS防范功能';

ALTER TABLE uk_spt_salespatter ADD delaytime int(11) DEFAULT 0 COMMENT '延迟挂断时间 （秒）';
ALTER TABLE uk_que_survey_process ADD delaytime int(11) DEFAULT 0 COMMENT '延迟挂断时间 （秒）';

CREATE TABLE `uk_contacts_item` (
     `id` varchar(32) NOT NULL COMMENT '主键ID',
     `name` varchar(100) DEFAULT NULL COMMENT '名称',
     `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
     `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
     `createtime` datetime DEFAULT NULL COMMENT '创建时间',
     `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
     `itemid` varchar(32) DEFAULT NULL COMMENT '项目id',
     `parentid` varchar(32) DEFAULT NULL COMMENT '父级id',
	 `type` varchar(255) DEFAULT NULL COMMENT '类型（联系人、客户）',
     `orgi` varchar(200) DEFAULT NULL COMMENT '租户id',
     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='联系人-项目分类表';

CREATE TABLE `uk_contacts_authorize` (
     `id` varchar(32) NOT NULL COMMENT '主键ID',
     `userid` varchar(32) DEFAULT NULL COMMENT '被授权人id',
	 `organ` varchar(32) DEFAULT NULL COMMENT '被授权人组织id',
     `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
     `createtime` datetime DEFAULT NULL COMMENT '创建时间',
     `itemid` varchar(32) DEFAULT NULL COMMENT '项目id',
     `orgi` varchar(200) DEFAULT NULL COMMENT '租户id',
	 `sup` tinyint(4) DEFAULT '0' COMMENT '是否管理员',
     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='联系人-项目分类-授权表';

ALTER TABLE uk_contacts ADD itemid varchar(32) DEFAULT NULL COMMENT '项目分类id';
ALTER TABLE uk_contacts_authorize ADD datastatus tinyint(4) DEFAULT '0' COMMENT '数据状态';

ALTER TABLE uk_que_result_answer ADD answertype varchar(50) DEFAULT NULL COMMENT '匹配答案类型';
ALTER TABLE uk_que_result_answer ADD answermsg text DEFAULT NULL COMMENT '回答内容';
ALTER TABLE uk_que_result_answer ADD answerexpmsg text DEFAULT NULL COMMENT '回答内容';

ALTER TABLE uk_contacts ADD cusname varchar(32) DEFAULT NULL COMMENT '客户名称';
ALTER TABLE uk_contacts ADD cusphone varchar(32) DEFAULT NULL COMMENT '客户-联系电话';
ALTER TABLE uk_contacts ADD cusemail varchar(32) DEFAULT NULL COMMENT '客户-电子邮件';
ALTER TABLE uk_contacts ADD cusprovince varchar(32) DEFAULT NULL COMMENT '客户-省份';
ALTER TABLE uk_contacts ADD cuscity varchar(32) DEFAULT NULL COMMENT '客户-城市';
ALTER TABLE uk_contacts ADD cusaddress varchar(32) DEFAULT NULL COMMENT '客户-客户地址';
ALTER TABLE uk_contacts ADD ekind varchar(32) DEFAULT NULL COMMENT '客户-客户类型';
ALTER TABLE uk_contacts ADD elevel varchar(32) DEFAULT NULL COMMENT '客户-客户级别';
ALTER TABLE uk_contacts ADD esource varchar(32) DEFAULT NULL COMMENT '客户-客户来源';
ALTER TABLE uk_contacts ADD maturity varchar(32) DEFAULT NULL COMMENT '客户-成熟度';
ALTER TABLE uk_contacts ADD industry varchar(32) DEFAULT NULL COMMENT '客户-行业';
ALTER TABLE uk_contacts ADD cusvalidstatus varchar(32) DEFAULT NULL COMMENT '客户-客户状态';
ALTER TABLE uk_contacts ADD cusdescription varchar(32) DEFAULT NULL COMMENT '客户-客户说明';

ALTER TABLE uk_xiaoe_config ADD code varchar(50) DEFAULT NULL ;


ALTER TABLE uk_tableproperties ADD sreplace tinyint(4) DEFAULT 0 COMMENT '启用字符串替换';
ALTER TABLE uk_tableproperties ADD curstr varchar(255) DEFAULT NULL COMMENT '待替换字符串';
ALTER TABLE uk_tableproperties ADD tarstr varchar(255) DEFAULT NULL COMMENT '替换后字符串';

ALTER TABLE uk_que_result_answer ADD matchkey varchar(500) DEFAULT NULL COMMENT '匹配中的词';
ALTER TABLE uk_que_result_answer ADD matchtype varchar(10) DEFAULT NULL COMMENT '匹配类型 matches indexof';

ALTER TABLE uk_contacts ADD workstatus varchar(32) DEFAULT NULL COMMENT '业务状态';
ALTER TABLE uk_contacts ADD callstatus varchar(32) DEFAULT NULL COMMENT '拨打状态';
ALTER TABLE uk_contacts ADD callresult varchar(32) DEFAULT NULL COMMENT '拨打结果';
ALTER TABLE uk_contacts ADD calltime varchar(50) DEFAULT NULL COMMENT '最后一次拨打时间';
ALTER TABLE uk_contacts ADD firstcalltimes varchar(50) DEFAULT NULL COMMENT '第一次拨打时间';
ALTER TABLE uk_contacts ADD firstcallstatus varchar(50) DEFAULT NULL COMMENT '第一次拨打状态';
ALTER TABLE uk_contacts ADD calltimes varchar(50) DEFAULT NULL COMMENT '拨打次数';
ALTER TABLE uk_contacts ADD ringtime varchar(50) DEFAULT NULL COMMENT '拨打时长';
ALTER TABLE uk_contacts ADD apstatus varchar(50) DEFAULT NULL COMMENT '';
ALTER TABLE uk_contacts ADD succcall varchar(50) DEFAULT NULL COMMENT '成功拨打次数';
ALTER TABLE uk_contacts ADD faildcall varchar(50) DEFAULT NULL COMMENT '失败拨打次数';
ALTER TABLE uk_contacts ADD incall varchar(50) DEFAULT NULL COMMENT '通话时长';
ALTER TABLE uk_contacts ADD status varchar(50) DEFAULT NULL COMMENT '分配状态';
ALTER TABLE uk_contacts ADD owneruser varchar(32) DEFAULT NULL COMMENT '分配坐席';
ALTER TABLE uk_contacts ADD ownerdept varchar(32) DEFAULT NULL COMMENT '分配部门';
ALTER TABLE uk_contacts ADD ownerai varchar(32) DEFAULT NULL COMMENT '分配机器人';
ALTER TABLE uk_contacts ADD ownerforecast varchar(32) DEFAULT NULL COMMENT '分配队列';
ALTER TABLE uk_contacts ADD actid varchar(32) DEFAULT NULL COMMENT '活动ID';
ALTER TABLE uk_contacts ADD distime datetime DEFAULT NULL COMMENT '分配时间';

CREATE TABLE `uk_contacts_relation` (
     `id` varchar(32) NOT NULL COMMENT '主键ID',
     `contactsid` varchar(32) DEFAULT NULL COMMENT '联系人id',
	 `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
     `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
     `createtime` datetime DEFAULT NULL COMMENT '创建时间',
     `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
     `orgi` varchar(200) DEFAULT NULL COMMENT '租户id',
	 `relateid` varchar(200) DEFAULT NULL COMMENT '关系联系人id',
	 `relation` varchar(200) DEFAULT NULL COMMENT '关系',
     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='联系人-关系表';

ALTER TABLE uk_contacts ADD discount int(11) DEFAULT '0' COMMENT '分配次数';

INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abf6c63016abfc5221303ac', '操作预警级别', 'pub', 'com.dic.reqlog.warnlv', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 16:32:08', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abe95c2016abeac22ff00ed', '拦截动作', 'pub', 'com.dic.reqlog.aciton', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 11:25:13', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abe72ed016abe809da6008f', '审计日志预警短信', 'pub', 'reqlogsms', 'ukewo', 'layui-icon', '297e63f05d1da6be015d1dae6de20002', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 10:37:41', NULL, 1, 0, '297e63f05d1da6be015d1dae6de20002', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816abe72ed016abe80eb3e0093', '审计日志预警邮件', 'pub', 'reqlogemail', 'ukewo', 'layui-icon', '297e63f05d1da6be015d1dae6de20002', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-16 10:38:01', NULL, 1, 0, '297e63f05d1da6be015d1dae6de20002', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0cca564e006d', '联系人关系', 'pub', 'com.dic.contacts.relation', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:28:35', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0ccdbe4b0097', '朋友关系', 'pub', 'friend', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:32:18', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0cce133c009e', '同学关系', 'pub', 'class', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:32:40', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0ccef7bd00b0', '师生关系', 'pub', 'stuandter', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:33:38', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0ccf5e1500b8', '雇佣关系', 'pub', 'employment', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:34:05', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0ccf93e400bf', '父子关系', 'pub', 'farson', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:34:18', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0ccfdbf000c6', '母子关系', 'pub', 'momson', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:34:37', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0cd02d4100cd', '兄妹关系', 'pub', 'brosis', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:34:58', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('402813816b0cc1e8016b0cd0b44100d9', '姐弟关系', 'pub', 'sisbro', 'ukewo', 'layui-icon', '402813816b0cc1e8016b0cca564e006d', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-5-31 15:35:32', NULL, 1, 0, '402813816b0cc1e8016b0cca564e006d', 0, 0, NULL, NULL, NULL, NULL, NULL);

ALTER TABLE uk_contacts ADD reservation tinyint(4) DEFAULT 0 COMMENT '是否预约';
ALTER TABLE uk_contacts ADD optime datetime DEFAULT NULL COMMENT '预约时间';
ALTER TABLE uk_contacts ADD salestatus varchar(32) DEFAULT NULL COMMENT '业务状态';
ALTER TABLE uk_contacts ADD salesmemo varchar(32) DEFAULT NULL COMMENT '业务备注';
ALTER TABLE uk_contacts ADD attachment tinyint(4) DEFAULT 0 COMMENT '联系人附件';
ALTER TABLE uk_contacts ADD processed tinyint(4) DEFAULT 0 COMMENT '预约已调度';
ALTER TABLE uk_contacts_item ADD statuscode varchar(32) DEFAULT NULL COMMENT '状态id';

ALTER TABLE uk_qc_template_item ADD wordstype text COMMENT '词库ID';
ALTER TABLE uk_qc_template_item ADD mcontain text COMMENT '必须包含';
ALTER TABLE uk_qc_template_item ADD ncontain text COMMENT '不可包含';
ALTER TABLE uk_jobdetail ADD autoquality tinyint(4) DEFAULT 0 COMMENT '是否开启自动质检（默认关闭）';
